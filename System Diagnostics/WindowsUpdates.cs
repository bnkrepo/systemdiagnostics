﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WUApiInterop;

namespace System_Diagnostics
{
    public class WindowsUpdates
    {

        public static List<string> GetWindowsUpdates()
        {
            List<string> list = new List<string>();
            var updateSession = new UpdateSession();
            var updateSearcher = updateSession.CreateUpdateSearcher();
            var count = updateSearcher.GetTotalHistoryCount();
            var history = updateSearcher.QueryHistory(0, count);

            for (int i = 0; i < count; ++i)
                list.Add(history[i].Date.ToString() + " " + history[i].Title);

            return list;
        }
    }
}
