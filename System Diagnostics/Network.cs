﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Management;
using System.Net.NetworkInformation;

namespace System_Diagnostics
{
    public class Network
    {
        #region Ports being used by processes

        //http://www.cheynewallace.com/get-active-ports-and-associated-process-names-in-c/
        // 

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Gets the net stat ports. </summary>
        ///
        /// <returns>   The net stat ports. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static List<Port> GetNetStatPorts()
        {
            var Ports = new List<Port>();

            try
            {
                using (Process p = new Process())
                {
                    ProcessStartInfo ps = new ProcessStartInfo();
                    ps.Arguments = "-a -n -o";
                    ps.FileName = "netstat.exe";
                    ps.UseShellExecute = false;
                    ps.WindowStyle = ProcessWindowStyle.Hidden;
                    ps.RedirectStandardInput = true;
                    ps.RedirectStandardOutput = true;
                    ps.RedirectStandardError = true;

                    p.StartInfo = ps;
                    p.Start();

                    StreamReader stdOutput = p.StandardOutput;
                    StreamReader stdError = p.StandardError;

                    string content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                    string exitStatus = p.ExitCode.ToString();

                    if (exitStatus != "0")
                    {
                        // Command Errored. Handle Here If Need Be
                    }

                    //Get The Rows
                    string[] rows = Regex.Split(content, "\r\n");

                    foreach (string row in rows)
                    {
                        //Split it baby
                        string[] tokens = Regex.Split(row, "\\s+");

                        if (tokens.Length > 4 && (tokens[1].Equals("UDP") || tokens[1].Equals("TCP")))
                        {
                            string localAddress = Regex.Replace(tokens[2], @"\[(.*?)\]", "1.1.1.1");

                            Ports.Add(new Port
                                        {
                                            Protocol = localAddress.Contains("1.1.1.1") ? String.Format("{0}v6", tokens[1]) : String.Format("{0}v4", tokens[1]),
                                            PortNumber = localAddress.Split(':')[1],
                                            ProcessName = tokens[1] == "UDP" ? LookupProcess(Convert.ToInt16(tokens[4])) : LookupProcess(Convert.ToInt16(tokens.Length == 6 ? tokens[5] : tokens[4])),
                                            Pid = Convert.ToInt16(tokens.Length == 6 ? tokens[5] : tokens[4])
                                        });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Ports != null && Ports.Count > 0)
                {
                    Ports = Ports.OrderBy(port => port.Pid).ToList();
                }
            }

          return Ports;
        }

        public static List<RemoteConnection> GetNetStatConnections()
        {
            var remoteConnections = new List<RemoteConnection>();

            try
            {
                using (Process p = new Process())
                {

                    ProcessStartInfo ps = new ProcessStartInfo();
                    ps.Arguments = "-a -n -o";
                    ps.FileName = "netstat.exe";
                    ps.UseShellExecute = false;
                    ps.WindowStyle = ProcessWindowStyle.Hidden;
                    ps.RedirectStandardInput = true;
                    ps.RedirectStandardOutput = true;
                    ps.RedirectStandardError = true;

                    p.StartInfo = ps;
                    p.Start();

                    StreamReader stdOutput = p.StandardOutput;
                    StreamReader stdError = p.StandardError;

                    string content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                    string exitStatus = p.ExitCode.ToString();

                    if (exitStatus != "0")
                    {
                        // Command Errored. Handle Here If Need Be
                    }

                    //Get The Rows
                    string[] rows = Regex.Split(content, "\r\n");

                    if (rows.Length == 2 && rows[0] == "The requested operation requires elevation.")
                    {
                        return remoteConnections;
                    }
                    else
                    {
                        foreach (string row in rows)
                        {
                            //Split it baby
                            string[] tokens = Regex.Split(row, "\\s+");

                            if (tokens.Length > 4 && (tokens[1].Equals("UDP") || tokens[1].Equals("TCP")))
                            {
                                string localAddress = Regex.Replace(tokens[2], @"\[(.*?)\]", "1.1.1.1");
                                string remoteAddress = Regex.Replace(tokens[3], @"\[(.*?)\]", "1.1.1.1");

                                remoteConnections.Add(new RemoteConnection
                                {
                                    Protocol = localAddress.Contains("1.1.1.1") ? String.Format("{0}v6", tokens[1]) : String.Format("{0}v4", tokens[1]),
                                    LocalAddress = localAddress,
                                    RemoteAddress = remoteAddress,
                                    ProcessName = tokens[1] == "UDP" ? LookupProcess(Convert.ToInt16(tokens[4])) : LookupProcess(Convert.ToInt16(tokens.Length == 6 ? tokens[5] : tokens[4])),
                                    Pid = Convert.ToInt16(tokens.Length == 6 ? tokens[5] : tokens[4])
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (remoteConnections != null && remoteConnections.Count > 0)
                {
                    remoteConnections = remoteConnections.OrderBy(port => port.Pid).ToList();
                }
            }

            return remoteConnections;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Looks up a given key to find its associated process. </summary>
        ///
        /// <param name="pid">  The pid. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        public static string LookupProcess(int pid) 
        {
          string procName;

          try 
          {
              procName = Process.GetProcessById(pid).ProcessName; 
          } 
          catch (Exception) 
          {
              procName = "-";
          }

          return procName;
        }
    
        #endregion

        #region Network Interface Enumeration

        public static List<NetworkInterface> GetNetInterfaceList()
        {
            List<NetworkInterface> netInterfaceList = new List<NetworkInterface>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_NetworkAdapterConfiguration");                

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    NetworkInterface netInterface = new NetworkInterface();

                    try{ netInterface.Description = queryObj["Description"].ToString();}catch(Exception){}
                    try{ netInterface.DHCPEnabled = queryObj["DHCPEnabled"].ToString();}catch(Exception){}
                    try{ netInterface.DNSHostName = queryObj["DNSHostName"].ToString();}catch(Exception){}

                    try
                    {
                        object ip = queryObj["IPAddress"];

                        if (ip != null)
                        {
                            if(!(ip is string[]))
                                netInterface.IPAddress = queryObj["IPAddress"].ToString();
                            else
                            {
                                netInterface.IPAddress = (ip as string[])[0];
                            }
                        }
                        
                       //     continue;
                    }
                    catch(Exception){}
                    
                    try
                    {
                        object subnet = queryObj["IPSubnet"];

                        if (subnet != null)
                        {
                            if (!(subnet is string[]))
                                netInterface.IPSubnet =  queryObj["IPSubnet"].ToString();
                            else
                            {
                                netInterface.IPSubnet = (subnet as string[])[0];
                            }
                        }
                    }
                    catch(Exception){}

                    try { netInterface.MACAddress = queryObj["MACAddress"].ToString(); }catch (Exception) { }

                    if (string.IsNullOrEmpty(netInterface.IPAddress) == false)
                        netInterfaceList.Add(netInterface);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return netInterfaceList;
        }

        #endregion

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Ping host. </summary>
        ///
        /// <param name="nameOrAddress">    The name or address. </param>
        ///
        /// <returns>   true if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();

            try
            {
                PingReply reply = pinger.Send(nameOrAddress);

                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }

            return pingable;
        }

    }

    public class NetworkInterface
    {
        public string Description { get; set; }
        public string DHCPEnabled { get; set; }
        public string DNSHostName { get; set; }
        public string IPAddress { get; set; }
        public string IPSubnet { get; set; }
        public string MACAddress { get; set; }

        public NetworkInterface()
        {

        }

    }
    /// <summary>   Port. </summary>
    public class Port
    {
        public string Name
        {
            get
            {
                return string.Format("{0} ({1} port {2})", this.ProcessName, this.Protocol, this.PortNumber);
            }
            set { }
        }

        public int Pid { get; set; }
        public string PortNumber { get; set; }
        public string ProcessName { get; set; }
        public string Protocol { get; set; }
    }

    public class RemoteConnection
    {
        public int Pid { get; set; }
        public string LocalAddress { get; set; }
        public string RemoteAddress { get; set; }
        public string ProcessName { get; set; }
        public string Protocol { get; set; }
        public string State{ get; set; }
    }
    
}
