﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace System_Diagnostics
{
    public class InstalledPrograms
    {
        public static List<InstalledProgramsInfo> GetInstalledProgramsInfo()
        {
            List<InstalledProgramsInfo> infoList = new List<InstalledProgramsInfo>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Product");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    InstalledProgramsInfo info = null;

                    try
                    {
                        info                    = new InstalledProgramsInfo();
                        info.Description        = queryObj["Description"].ToString();
                        info.IdentifyingNumber  = queryObj["IdentifyingNumber"].ToString();
                        try{info.InstallDate    = ManagementDateTimeConverter.ToDateTime(queryObj["InstallDate"].ToString()).ToShortDateString();} catch(Exception){}
                        try{ info.InstallLocation = queryObj["InstallLocation"].ToString(); }catch (Exception) { }
                        try{info.LocalPackage   = queryObj["LocalPackage"].ToString();}catch (Exception) { }
                        info.Name               = queryObj["Name"].ToString();
                        info.PackageCode        = queryObj["PackageCode"].ToString();
                        info.PackageName        = queryObj["PackageName"].ToString();
                        info.Vendor             = queryObj["Vendor"].ToString();
                        info.Version            = queryObj["Version"].ToString();
                    }
                    catch (Exception)
                    {

                    }

                    if(info != null)
                        infoList.Add(info);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return infoList;
        }
    }

    public class InstalledProgramsInfo
    {
        public string Caption { get; set; }
        public string Description { get; set; }
        public string IdentifyingNumber { get; set; }
        public string InstallDate { get; set; }
        public string InstallLocation { get; set; }
        public string LocalPackage { get; set; }
        public string Name { get; set; }
        public string PackageCache { get; set; }
        public string PackageCode { get; set; }
        public string PackageName { get; set; }
        public string Vendor { get; set; }
        public string Version { get; set; }
    }
}
