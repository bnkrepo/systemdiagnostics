﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace System_Diagnostics
{
    public partial class IPControl : UserControl
    {
        bool _invalidKeyPressed = false;
        bool _enableSubnetChange = true;

        public bool EnableSubnetChange 
        {
            get
            {
                return _enableSubnetChange;
            }

            set
            {
                _enableSubnetChange = txt1.Enabled = txt2.Enabled = txt3.Enabled = value;
            }
        }

        public string IP
        {
            get 
            {
                return String.Format("{0}.{1}.{2}.{3}", txt1.Text, txt2.Text, txt3.Text, txt4.Text);
            }

            set
            {
                txt1.Text = txt2.Text = txt3.Text = txt4.Text = "";
                if(string.IsNullOrEmpty(value) == false)                
                {
                    string[] ip = value.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                    if (ip.Length == 4)
                    {
                        txt1.Text = ValidateIP(ip[0]);
                        txt2.Text = ValidateIP(ip[1]);
                        txt3.Text = ValidateIP(ip[2]);
                        txt4.Text = ValidateIP(ip[3]);
                    }
                    else if (ip.Length == 3)
                    {
                        txt1.Text = ValidateIP(ip[0]);
                        txt2.Text = ValidateIP(ip[1]);
                        txt3.Text = ValidateIP(ip[2]);
                        txt4.Text = "1";
                    }
                }
            }
        }

        private string ValidateIP(string str)
        {
            if (string.IsNullOrEmpty(str) == true)
                return "";

            int n = Int32.Parse(str);

            if (n < 1 && n > 255)
                return "";
            else
                return str;

        }

        public IPControl()
        {
            InitializeComponent();
        }

        private void txt1_KeyDown(object sender, KeyEventArgs e)
        {
            base.OnKeyDown(e);

            _invalidKeyPressed = false;

            if (e.KeyData == Keys.Decimal)
            {
                if ((sender as TextBox) == txt1)
                    txt2.Focus();
                else if ((sender as TextBox) == txt2)
                    txt3.Focus();
                else if ((sender as TextBox) == txt3)
                    txt4.Focus();

                _invalidKeyPressed = true;
            }
            else if (e.KeyValue < '0' && e.KeyValue > '9')
                _invalidKeyPressed = true;
            else
            {
                if (string.IsNullOrEmpty(ValidateIP((sender as TextBox).Text)) == true)
                    _invalidKeyPressed = true;

                var key = new KeysConverter().ConvertToString(e.KeyData);

                _invalidKeyPressed = false;
            }            
        }

        private void txt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_invalidKeyPressed == true)
                e.Handled = true;
        }
    }
}
