﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace System_Diagnostics
{
    public class CPUInfo
    {
        public static CPU GetCPUInfo()
        {
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_Processor");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    CPU cpu = new CPU
                    {
                        AddressWidth = queryObj["AddressWidth"].ToString(),
                        Architecture = queryObj["Architecture"].ToString(),
                        Availability = queryObj["Availability"].ToString(),
                        Caption = queryObj["Caption"].ToString(),
                        CpuStatus = queryObj["CpuStatus"].ToString(),
                        CurrentClockSpeed = queryObj["CurrentClockSpeed"].ToString(),
                        CurrentVoltage = queryObj["CurrentVoltage"].ToString(),
                        DataWidth = queryObj["DataWidth"].ToString(),
                        Description = queryObj["Description"].ToString(),
                        Manufacturer = queryObj["Manufacturer"].ToString(),
                        MaxClockSpeed = queryObj["MaxClockSpeed"].ToString(),
                        Name = queryObj["Name"].ToString(),
                        NumberOfCores = queryObj["NumberOfCores"].ToString(),
                        NumberOfLogicalProcessors = queryObj["NumberOfLogicalProcessors"].ToString()
                    };

                    return cpu;
                }
            }
            catch (ManagementException)
            {

            }

            return null;
        }
    }

    public class CPU
    {
        public string AddressWidth { get; set; }
        public string Architecture { get; set; }
        public string Availability { get; set; }
        public string Caption { get; set; }
        public string CpuStatus { get; set; }
        public string CurrentClockSpeed { get; set; }
        public string CurrentVoltage { get; set; }
        public string DataWidth { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string MaxClockSpeed { get; set; }
        public string Name { get; set; }
        public string NumberOfCores { get; set; }
        public string NumberOfLogicalProcessors { get; set; }

        public CPU()
        {

        }
    }
}
