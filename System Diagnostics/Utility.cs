﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace System_Diagnostics
{
    public class Utility
    {
        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Format bytes. </summary>
        ///
        /// <param name="bytes">    The bytes. </param>
        ///
        /// <returns>   The formatted bytes. </returns>
        ///-------------------------------------------------------------------------------------------------

        public static string FormatBytes(ulong bytes)
        {
            string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
            int i;
            double dblSByte = bytes;
            for (i = 0; i < Suffix.Length && bytes >= 1024; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }

            return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Executes the command operation. </summary>
        ///
        /// <param name="strCommand">   The command. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        public static void ExecuteCommand(string strCommand)
        {
            try
            {
                Process.Start(strCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Executes the command operation. </summary>
        ///
        /// <param name="strCommand">   The command. </param>
        /// <param name="param">        The parameter. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        public static void ExecuteCommand(string strCommand, string param, string strWorkingDirectory = "")
        {
            try
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = strCommand;                
                psi.Arguments = param;

                if(string.IsNullOrEmpty(strWorkingDirectory) == false)
                    psi.WorkingDirectory = strWorkingDirectory;
                
                Process.Start(psi);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Query if 'addr' is valid ip. </summary>
        ///
        /// <param name="addr"> The address. </param>
        ///
        /// <returns>   true if valid ip, false if not. </returns>
        ///-------------------------------------------------------------------------------------------------

        public bool IsValidIP(string addr)
        {
            //create our match pattern
            string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            //create our Regular Expression object
            Regex check = new Regex(pattern);
            //boolean variable to hold the status
            bool valid = false;
            //check to make sure an ip address was provided
            if (addr == "")
            {
                //no address provided so return false
                valid = false;
            }
            else
            {
                //address provided so use the IsMatch Method
                //of the Regular Expression object
                valid = check.IsMatch(addr, 0);
            }
            //return the results
            return valid;
        }
    }
}
