﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace System_Diagnostics
{
    public class HardDisk
    {
        public static List<HardDiskInfo> GetHDDInfo()
        {
            List<HardDiskInfo> infoList = new List<HardDiskInfo>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_LogicalDisk");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    HardDiskInfo info = new HardDiskInfo();
                    
                    try{info.Access = queryObj["Access"].ToString();}catch(Exception){}
                    try{info.Caption = queryObj["Caption"].ToString();}catch(Exception){}
                    try{info.Compressed = queryObj["Compressed"].ToString();}catch(Exception){}
                    try{info.Description = queryObj["Description"].ToString();}catch(Exception){}
                    try{info.DeviceID = queryObj["DeviceID"].ToString();}catch(Exception){}
                    try{info.DriveType = queryObj["DriveType"].ToString();}catch(Exception){}
                    try{info.FileSystem = queryObj["FileSystem"].ToString();}catch(Exception){}
                    try { info.FreeSpace = Utility.FormatBytes((ulong)queryObj["FreeSpace"]); } catch (Exception) { }
                    try{info.Name = queryObj["Name"].ToString();}catch(Exception){}
                    try{info.Size = Utility.FormatBytes((ulong)queryObj["Size"]);}catch(Exception){}
                    try{info.VolumeName = queryObj["VolumeName"].ToString();}catch(Exception){}
                    try { info.VolumeSerialNumber = queryObj["VolumeSerialNumber"].ToString(); } catch (Exception) { }
                    try { info.ProviderName = queryObj["ProviderName"].ToString(); } catch (Exception) { }

                    infoList.Add(info);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return infoList;
        }
    }

    public class HardDiskInfo
    {
        public string Access{get; set;}
        public string Caption{get; set;}
        public string Compressed{get; set;}
        public string Description{get; set;}
        public string DeviceID{get; set;}
        public string DriveType{get; set;}
        public string FileSystem{get; set;}
        public string FreeSpace{get; set;}
        public string Name{get; set;}
        public string Size { get; set; }
        public string VolumeName { get; set; }
        public string VolumeSerialNumber { get; set; }
        public string ProviderName { get; set; }

        public HardDiskInfo()
        {
            Access = "-";
            Caption = "-";
            Compressed = "-";
            Description = "-";
            DeviceID = "-";
            DriveType = "-";
            FileSystem = "-";
            FreeSpace = "-";
            Name = "-";
            Size = "-";
            VolumeName = "-";
            VolumeSerialNumber = "-";
            ProviderName = "-";
        }
    }
}
