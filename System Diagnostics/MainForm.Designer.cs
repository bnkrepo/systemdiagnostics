﻿namespace System_Diagnostics
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabSystem = new System.Windows.Forms.TabPage();
            this.btnSystemReport = new System.Windows.Forms.Button();
            this.btnRelbMonitor = new System.Windows.Forms.Button();
            this.btnWinUpdate = new System.Windows.Forms.Button();
            this.lblInternet = new System.Windows.Forms.Label();
            this.btnOpenSysInfo = new System.Windows.Forms.Button();
            this.btnOpenFirewall = new System.Windows.Forms.Button();
            this.btnOpenSystem = new System.Windows.Forms.Button();
            this.txtSystem = new System.Windows.Forms.TextBox();
            this.tabStartupItems = new System.Windows.Forms.TabPage();
            this.btnSysConfig1 = new System.Windows.Forms.Button();
            this.txtStartupItems = new System.Windows.Forms.TextBox();
            this.tabHDD = new System.Windows.Forms.TabPage();
            this.btnDiskDefrag = new System.Windows.Forms.Button();
            this.btnDiskCleanup = new System.Windows.Forms.Button();
            this.txtHDD = new System.Windows.Forms.TextBox();
            this.tabNetwork = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPathPing = new System.Windows.Forms.Button();
            this.btnOpenHostsFile = new System.Windows.Forms.Button();
            this.btnIpConfig = new System.Windows.Forms.Button();
            this.txtPingHost = new System.Windows.Forms.TextBox();
            this.btnPing = new System.Windows.Forms.Button();
            this.btnNWCEnter = new System.Windows.Forms.Button();
            this.txtNetInterfaces = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnRefreshNwOpenPorts = new System.Windows.Forms.Button();
            this.txtNetPorts = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtNetConnections = new System.Windows.Forms.TextBox();
            this.tabProcesses = new System.Windows.Forms.TabPage();
            this.btnResourceMonitor = new System.Windows.Forms.Button();
            this.btnTaskManager = new System.Windows.Forms.Button();
            this.txtProcesses = new System.Windows.Forms.TextBox();
            this.tabServices = new System.Windows.Forms.TabPage();
            this.btnServices = new System.Windows.Forms.Button();
            this.txtServices = new System.Windows.Forms.TextBox();
            this.tabRuntimes = new System.Windows.Forms.TabPage();
            this.txtRuntime = new System.Windows.Forms.TextBox();
            this.tabEnvironmentVariables = new System.Windows.Forms.TabPage();
            this.txtEnvVariable = new System.Windows.Forms.TextBox();
            this.tabInstalledSoftware = new System.Windows.Forms.TabPage();
            this.btnUninstall = new System.Windows.Forms.Button();
            this.txtPrograms = new System.Windows.Forms.TextBox();
            this.tabHotFixes = new System.Windows.Forms.TabPage();
            this.btnWindowsUpdates = new System.Windows.Forms.Button();
            this.txtHotFixes = new System.Windows.Forms.TextBox();
            this.tabTools = new System.Windows.Forms.TabPage();
            this.btnServices1 = new System.Windows.Forms.Button();
            this.btnRegedit = new System.Windows.Forms.Button();
            this.btnMobilityCenter = new System.Windows.Forms.Button();
            this.btnCredentialManager = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.lnkLblOther = new System.Windows.Forms.LinkLabel();
            this.lnkLblKaspersky = new System.Windows.Forms.LinkLabel();
            this.lnkLblAvira = new System.Windows.Forms.LinkLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnBackupAndRestore = new System.Windows.Forms.Button();
            this.btnMasterControlPanel = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTroubleshooting = new System.Windows.Forms.Button();
            this.btnPerformDiag = new System.Windows.Forms.Button();
            this.btnGenSysReport = new System.Windows.Forms.Button();
            this.btnReliabilityMonitor = new System.Windows.Forms.Button();
            this.btnSystemMaintenance1 = new System.Windows.Forms.Button();
            this.btnPSR = new System.Windows.Forms.Button();
            this.btnPerfMon = new System.Windows.Forms.Button();
            this.btnFolderOptions = new System.Windows.Forms.Button();
            this.btnResourceMon = new System.Windows.Forms.Button();
            this.btnTaskMgr = new System.Windows.Forms.Button();
            this.btnEventViewer = new System.Windows.Forms.Button();
            this.btnAboutWindows = new System.Windows.Forms.Button();
            this.btnSysConfig = new System.Windows.Forms.Button();
            this.btnSysInfo = new System.Windows.Forms.Button();
            this.btnRemoteDesktop = new System.Windows.Forms.Button();
            this.btnDeviceMgr = new System.Windows.Forms.Button();
            this.btnAdminTools = new System.Windows.Forms.Button();
            this.btnControlPanel = new System.Windows.Forms.Button();
            this.btnCmd = new System.Windows.Forms.Button();
            this.tabAbout = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.txtDebug = new System.Windows.Forms.TextBox();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.txtError = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bgwNetworkPorts = new System.ComponentModel.BackgroundWorker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnSaveAll = new System.Windows.Forms.Button();
            this.btnCopyToCLipboard = new System.Windows.Forms.Button();
            this.bgwOS = new System.ComponentModel.BackgroundWorker();
            this.timerPing = new System.Windows.Forms.Timer(this.components);
            this.bgwHDD = new System.ComponentModel.BackgroundWorker();
            this.bgwNetwork = new System.ComponentModel.BackgroundWorker();
            this.bgwInstalledPrograms = new System.ComponentModel.BackgroundWorker();
            this.bgwProcessInfo = new System.ComponentModel.BackgroundWorker();
            this.bgwRuntime = new System.ComponentModel.BackgroundWorker();
            this.bgwEnvironmentVars = new System.ComponentModel.BackgroundWorker();
            this.bgwHotFixes = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.bgwNetworkConnections = new System.ComponentModel.BackgroundWorker();
            this.bgwStartupItems = new System.ComponentModel.BackgroundWorker();
            this.ipControlTo = new System_Diagnostics.IPControl();
            this.ipControlFrom = new System_Diagnostics.IPControl();
            this.bgwServices = new System.ComponentModel.BackgroundWorker();
            this.tabDevices = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabSystem.SuspendLayout();
            this.tabStartupItems.SuspendLayout();
            this.tabHDD.SuspendLayout();
            this.tabNetwork.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabProcesses.SuspendLayout();
            this.tabServices.SuspendLayout();
            this.tabRuntimes.SuspendLayout();
            this.tabEnvironmentVariables.SuspendLayout();
            this.tabInstalledSoftware.SuspendLayout();
            this.tabHotFixes.SuspendLayout();
            this.tabTools.SuspendLayout();
            this.tabAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl3.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabSystem);
            this.tabControl1.Controls.Add(this.tabStartupItems);
            this.tabControl1.Controls.Add(this.tabHDD);
            this.tabControl1.Controls.Add(this.tabNetwork);
            this.tabControl1.Controls.Add(this.tabProcesses);
            this.tabControl1.Controls.Add(this.tabServices);
            this.tabControl1.Controls.Add(this.tabRuntimes);
            this.tabControl1.Controls.Add(this.tabEnvironmentVariables);
            this.tabControl1.Controls.Add(this.tabInstalledSoftware);
            this.tabControl1.Controls.Add(this.tabHotFixes);
            this.tabControl1.Controls.Add(this.tabTools);
            this.tabControl1.Controls.Add(this.tabAbout);
            this.tabControl1.Controls.Add(this.tabDevices);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(968, 597);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabSystem
            // 
            this.tabSystem.Controls.Add(this.btnSystemReport);
            this.tabSystem.Controls.Add(this.btnRelbMonitor);
            this.tabSystem.Controls.Add(this.btnWinUpdate);
            this.tabSystem.Controls.Add(this.lblInternet);
            this.tabSystem.Controls.Add(this.btnOpenSysInfo);
            this.tabSystem.Controls.Add(this.btnOpenFirewall);
            this.tabSystem.Controls.Add(this.btnOpenSystem);
            this.tabSystem.Controls.Add(this.txtSystem);
            this.tabSystem.Location = new System.Drawing.Point(4, 22);
            this.tabSystem.Name = "tabSystem";
            this.tabSystem.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystem.Size = new System.Drawing.Size(960, 571);
            this.tabSystem.TabIndex = 2;
            this.tabSystem.Text = "System";
            this.tabSystem.UseVisualStyleBackColor = true;
            // 
            // btnSystemReport
            // 
            this.btnSystemReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSystemReport.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSystemReport.Image = ((System.Drawing.Image)(resources.GetObject("btnSystemReport.Image")));
            this.btnSystemReport.Location = new System.Drawing.Point(766, 230);
            this.btnSystemReport.Name = "btnSystemReport";
            this.btnSystemReport.Size = new System.Drawing.Size(188, 50);
            this.btnSystemReport.TabIndex = 6;
            this.btnSystemReport.Text = "Generate System Report";
            this.btnSystemReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSystemReport.UseVisualStyleBackColor = true;
            this.btnSystemReport.Click += new System.EventHandler(this.btnSystemReport_Click);
            // 
            // btnRelbMonitor
            // 
            this.btnRelbMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRelbMonitor.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelbMonitor.Image = ((System.Drawing.Image)(resources.GetObject("btnRelbMonitor.Image")));
            this.btnRelbMonitor.Location = new System.Drawing.Point(766, 194);
            this.btnRelbMonitor.Name = "btnRelbMonitor";
            this.btnRelbMonitor.Size = new System.Drawing.Size(188, 30);
            this.btnRelbMonitor.TabIndex = 5;
            this.btnRelbMonitor.Text = "Reliability Monitor";
            this.btnRelbMonitor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRelbMonitor.UseVisualStyleBackColor = true;
            this.btnRelbMonitor.Click += new System.EventHandler(this.btnRelbMonitor_Click);
            // 
            // btnWinUpdate
            // 
            this.btnWinUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWinUpdate.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWinUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnWinUpdate.Image")));
            this.btnWinUpdate.Location = new System.Drawing.Point(766, 114);
            this.btnWinUpdate.Name = "btnWinUpdate";
            this.btnWinUpdate.Size = new System.Drawing.Size(188, 30);
            this.btnWinUpdate.TabIndex = 4;
            this.btnWinUpdate.Text = "Windows Update";
            this.btnWinUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnWinUpdate.UseVisualStyleBackColor = true;
            this.btnWinUpdate.Click += new System.EventHandler(this.btnWinUpdate_Click);
            // 
            // lblInternet
            // 
            this.lblInternet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInternet.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInternet.Location = new System.Drawing.Point(766, 542);
            this.lblInternet.Name = "lblInternet";
            this.lblInternet.Size = new System.Drawing.Size(188, 23);
            this.lblInternet.TabIndex = 6;
            this.lblInternet.Text = "---";
            this.lblInternet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOpenSysInfo
            // 
            this.btnOpenSysInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenSysInfo.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenSysInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenSysInfo.Image")));
            this.btnOpenSysInfo.Location = new System.Drawing.Point(766, 42);
            this.btnOpenSysInfo.Name = "btnOpenSysInfo";
            this.btnOpenSysInfo.Size = new System.Drawing.Size(188, 30);
            this.btnOpenSysInfo.TabIndex = 2;
            this.btnOpenSysInfo.Text = "System Information";
            this.btnOpenSysInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenSysInfo.UseVisualStyleBackColor = true;
            this.btnOpenSysInfo.Click += new System.EventHandler(this.btnOpenSysInfo_Click);
            // 
            // btnOpenFirewall
            // 
            this.btnOpenFirewall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenFirewall.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFirewall.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFirewall.Image")));
            this.btnOpenFirewall.Location = new System.Drawing.Point(766, 78);
            this.btnOpenFirewall.Name = "btnOpenFirewall";
            this.btnOpenFirewall.Size = new System.Drawing.Size(188, 30);
            this.btnOpenFirewall.TabIndex = 3;
            this.btnOpenFirewall.Text = "Windows Firewall";
            this.btnOpenFirewall.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenFirewall.UseVisualStyleBackColor = true;
            this.btnOpenFirewall.Click += new System.EventHandler(this.btnOpenFirewall_Click);
            // 
            // btnOpenSystem
            // 
            this.btnOpenSystem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenSystem.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenSystem.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenSystem.Image")));
            this.btnOpenSystem.Location = new System.Drawing.Point(766, 6);
            this.btnOpenSystem.Name = "btnOpenSystem";
            this.btnOpenSystem.Size = new System.Drawing.Size(188, 30);
            this.btnOpenSystem.TabIndex = 1;
            this.btnOpenSystem.Text = "System";
            this.btnOpenSystem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenSystem.UseVisualStyleBackColor = true;
            this.btnOpenSystem.Click += new System.EventHandler(this.btnOpenSystem_Click);
            // 
            // txtSystem
            // 
            this.txtSystem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSystem.BackColor = System.Drawing.SystemColors.Window;
            this.txtSystem.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSystem.Location = new System.Drawing.Point(6, 6);
            this.txtSystem.Multiline = true;
            this.txtSystem.Name = "txtSystem";
            this.txtSystem.ReadOnly = true;
            this.txtSystem.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSystem.Size = new System.Drawing.Size(754, 559);
            this.txtSystem.TabIndex = 0;
            this.txtSystem.Text = "Loading system data...";
            this.txtSystem.WordWrap = false;
            // 
            // tabStartupItems
            // 
            this.tabStartupItems.Controls.Add(this.btnSysConfig1);
            this.tabStartupItems.Controls.Add(this.txtStartupItems);
            this.tabStartupItems.Location = new System.Drawing.Point(4, 22);
            this.tabStartupItems.Name = "tabStartupItems";
            this.tabStartupItems.Padding = new System.Windows.Forms.Padding(3);
            this.tabStartupItems.Size = new System.Drawing.Size(960, 571);
            this.tabStartupItems.TabIndex = 12;
            this.tabStartupItems.Text = "Startup Items";
            this.tabStartupItems.UseVisualStyleBackColor = true;
            // 
            // btnSysConfig1
            // 
            this.btnSysConfig1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSysConfig1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSysConfig1.Location = new System.Drawing.Point(766, 6);
            this.btnSysConfig1.Name = "btnSysConfig1";
            this.btnSysConfig1.Size = new System.Drawing.Size(188, 30);
            this.btnSysConfig1.TabIndex = 13;
            this.btnSysConfig1.Text = "System Con&figuration";
            this.btnSysConfig1.UseVisualStyleBackColor = true;
            this.btnSysConfig1.Click += new System.EventHandler(this.btnSysConfig_Click);
            // 
            // txtStartupItems
            // 
            this.txtStartupItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStartupItems.BackColor = System.Drawing.SystemColors.Window;
            this.txtStartupItems.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStartupItems.Location = new System.Drawing.Point(6, 6);
            this.txtStartupItems.Multiline = true;
            this.txtStartupItems.Name = "txtStartupItems";
            this.txtStartupItems.ReadOnly = true;
            this.txtStartupItems.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStartupItems.Size = new System.Drawing.Size(754, 559);
            this.txtStartupItems.TabIndex = 1;
            this.txtStartupItems.Text = "Loading startup items...";
            this.txtStartupItems.WordWrap = false;
            // 
            // tabHDD
            // 
            this.tabHDD.Controls.Add(this.btnDiskDefrag);
            this.tabHDD.Controls.Add(this.btnDiskCleanup);
            this.tabHDD.Controls.Add(this.txtHDD);
            this.tabHDD.Location = new System.Drawing.Point(4, 22);
            this.tabHDD.Name = "tabHDD";
            this.tabHDD.Size = new System.Drawing.Size(960, 571);
            this.tabHDD.TabIndex = 3;
            this.tabHDD.Text = "HDD";
            this.tabHDD.UseVisualStyleBackColor = true;
            // 
            // btnDiskDefrag
            // 
            this.btnDiskDefrag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskDefrag.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiskDefrag.Image = ((System.Drawing.Image)(resources.GetObject("btnDiskDefrag.Image")));
            this.btnDiskDefrag.Location = new System.Drawing.Point(766, 42);
            this.btnDiskDefrag.Name = "btnDiskDefrag";
            this.btnDiskDefrag.Size = new System.Drawing.Size(188, 30);
            this.btnDiskDefrag.TabIndex = 2;
            this.btnDiskDefrag.Text = "Disk Def&ragment";
            this.btnDiskDefrag.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDiskDefrag.UseVisualStyleBackColor = true;
            this.btnDiskDefrag.Click += new System.EventHandler(this.btnDiskDefrag_Click);
            // 
            // btnDiskCleanup
            // 
            this.btnDiskCleanup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDiskCleanup.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiskCleanup.Image = ((System.Drawing.Image)(resources.GetObject("btnDiskCleanup.Image")));
            this.btnDiskCleanup.Location = new System.Drawing.Point(766, 6);
            this.btnDiskCleanup.Name = "btnDiskCleanup";
            this.btnDiskCleanup.Size = new System.Drawing.Size(188, 30);
            this.btnDiskCleanup.TabIndex = 1;
            this.btnDiskCleanup.Text = "&Disk Cleanup";
            this.btnDiskCleanup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDiskCleanup.UseVisualStyleBackColor = true;
            this.btnDiskCleanup.Click += new System.EventHandler(this.btnDiskCleanup_Click);
            // 
            // txtHDD
            // 
            this.txtHDD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHDD.BackColor = System.Drawing.SystemColors.Window;
            this.txtHDD.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHDD.Location = new System.Drawing.Point(6, 6);
            this.txtHDD.Multiline = true;
            this.txtHDD.Name = "txtHDD";
            this.txtHDD.ReadOnly = true;
            this.txtHDD.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtHDD.Size = new System.Drawing.Size(754, 559);
            this.txtHDD.TabIndex = 0;
            this.txtHDD.Text = "Loading hdd data...";
            // 
            // tabNetwork
            // 
            this.tabNetwork.Controls.Add(this.tabControl2);
            this.tabNetwork.Location = new System.Drawing.Point(4, 22);
            this.tabNetwork.Name = "tabNetwork";
            this.tabNetwork.Size = new System.Drawing.Size(960, 571);
            this.tabNetwork.TabIndex = 7;
            this.tabNetwork.Text = "Networking";
            this.tabNetwork.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Location = new System.Drawing.Point(3, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(954, 562);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.btnPathPing);
            this.tabPage1.Controls.Add(this.btnOpenHostsFile);
            this.tabPage1.Controls.Add(this.btnIpConfig);
            this.tabPage1.Controls.Add(this.txtPingHost);
            this.tabPage1.Controls.Add(this.btnPing);
            this.tabPage1.Controls.Add(this.btnNWCEnter);
            this.tabPage1.Controls.Add(this.txtNetInterfaces);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(946, 536);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Interfaces";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(752, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "IP:";
            // 
            // btnPathPing
            // 
            this.btnPathPing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPathPing.Enabled = false;
            this.btnPathPing.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.btnPathPing.Location = new System.Drawing.Point(848, 37);
            this.btnPathPing.Name = "btnPathPing";
            this.btnPathPing.Size = new System.Drawing.Size(92, 30);
            this.btnPathPing.TabIndex = 3;
            this.btnPathPing.Text = "Pat&h Ping";
            this.btnPathPing.UseVisualStyleBackColor = true;
            this.btnPathPing.Click += new System.EventHandler(this.btnPathPing_Click);
            // 
            // btnOpenHostsFile
            // 
            this.btnOpenHostsFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenHostsFile.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenHostsFile.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenHostsFile.Image")));
            this.btnOpenHostsFile.Location = new System.Drawing.Point(752, 177);
            this.btnOpenHostsFile.Name = "btnOpenHostsFile";
            this.btnOpenHostsFile.Size = new System.Drawing.Size(188, 30);
            this.btnOpenHostsFile.TabIndex = 6;
            this.btnOpenHostsFile.Text = "Open hosts File";
            this.btnOpenHostsFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenHostsFile.UseVisualStyleBackColor = true;
            this.btnOpenHostsFile.Click += new System.EventHandler(this.btnOpenHostsFile_Click);
            // 
            // btnIpConfig
            // 
            this.btnIpConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIpConfig.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIpConfig.Image = ((System.Drawing.Image)(resources.GetObject("btnIpConfig.Image")));
            this.btnIpConfig.Location = new System.Drawing.Point(752, 141);
            this.btnIpConfig.Name = "btnIpConfig";
            this.btnIpConfig.Size = new System.Drawing.Size(188, 30);
            this.btnIpConfig.TabIndex = 5;
            this.btnIpConfig.Text = "ipconfig";
            this.btnIpConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIpConfig.UseVisualStyleBackColor = true;
            this.btnIpConfig.Click += new System.EventHandler(this.btnIpConfig_Click);
            // 
            // txtPingHost
            // 
            this.txtPingHost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPingHost.Location = new System.Drawing.Point(789, 11);
            this.txtPingHost.Name = "txtPingHost";
            this.txtPingHost.Size = new System.Drawing.Size(151, 20);
            this.txtPingHost.TabIndex = 1;
            this.txtPingHost.TextChanged += new System.EventHandler(this.txtPingHost_TextChanged);
            // 
            // btnPing
            // 
            this.btnPing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPing.Enabled = false;
            this.btnPing.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPing.Location = new System.Drawing.Point(752, 37);
            this.btnPing.Name = "btnPing";
            this.btnPing.Size = new System.Drawing.Size(90, 30);
            this.btnPing.TabIndex = 2;
            this.btnPing.Text = "Ping";
            this.btnPing.UseVisualStyleBackColor = true;
            this.btnPing.Click += new System.EventHandler(this.btnPing_Click);
            // 
            // btnNWCEnter
            // 
            this.btnNWCEnter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNWCEnter.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNWCEnter.Image = ((System.Drawing.Image)(resources.GetObject("btnNWCEnter.Image")));
            this.btnNWCEnter.Location = new System.Drawing.Point(752, 89);
            this.btnNWCEnter.Name = "btnNWCEnter";
            this.btnNWCEnter.Size = new System.Drawing.Size(188, 46);
            this.btnNWCEnter.TabIndex = 4;
            this.btnNWCEnter.Text = "&Network and Sharing Center";
            this.btnNWCEnter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNWCEnter.UseVisualStyleBackColor = true;
            this.btnNWCEnter.Click += new System.EventHandler(this.btnNWCEnter_Click);
            // 
            // txtNetInterfaces
            // 
            this.txtNetInterfaces.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNetInterfaces.BackColor = System.Drawing.SystemColors.Window;
            this.txtNetInterfaces.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetInterfaces.Location = new System.Drawing.Point(3, 6);
            this.txtNetInterfaces.Multiline = true;
            this.txtNetInterfaces.Name = "txtNetInterfaces";
            this.txtNetInterfaces.ReadOnly = true;
            this.txtNetInterfaces.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNetInterfaces.Size = new System.Drawing.Size(743, 527);
            this.txtNetInterfaces.TabIndex = 0;
            this.txtNetInterfaces.Text = "Loading networking data...";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnRefreshNwOpenPorts);
            this.tabPage2.Controls.Add(this.txtNetPorts);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(946, 536);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Open Ports";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnRefreshNwOpenPorts
            // 
            this.btnRefreshNwOpenPorts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshNwOpenPorts.Location = new System.Drawing.Point(865, 500);
            this.btnRefreshNwOpenPorts.Name = "btnRefreshNwOpenPorts";
            this.btnRefreshNwOpenPorts.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshNwOpenPorts.TabIndex = 2;
            this.btnRefreshNwOpenPorts.Text = "&Refresh";
            this.btnRefreshNwOpenPorts.UseVisualStyleBackColor = true;
            this.btnRefreshNwOpenPorts.Click += new System.EventHandler(this.btnRefreshNwOpenPorts_Click);
            // 
            // txtNetPorts
            // 
            this.txtNetPorts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNetPorts.BackColor = System.Drawing.SystemColors.Window;
            this.txtNetPorts.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetPorts.Location = new System.Drawing.Point(3, 6);
            this.txtNetPorts.Multiline = true;
            this.txtNetPorts.Name = "txtNetPorts";
            this.txtNetPorts.ReadOnly = true;
            this.txtNetPorts.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNetPorts.Size = new System.Drawing.Size(940, 488);
            this.txtNetPorts.TabIndex = 1;
            this.txtNetPorts.Text = "Loading network port data...";
            this.txtNetPorts.WordWrap = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtNetConnections);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(946, 536);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Connections";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtNetConnections
            // 
            this.txtNetConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNetConnections.BackColor = System.Drawing.SystemColors.Window;
            this.txtNetConnections.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetConnections.Location = new System.Drawing.Point(3, 6);
            this.txtNetConnections.Multiline = true;
            this.txtNetConnections.Name = "txtNetConnections";
            this.txtNetConnections.ReadOnly = true;
            this.txtNetConnections.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNetConnections.Size = new System.Drawing.Size(940, 488);
            this.txtNetConnections.TabIndex = 2;
            this.txtNetConnections.Text = "Loading network port data...";
            this.txtNetConnections.WordWrap = false;
            // 
            // tabProcesses
            // 
            this.tabProcesses.Controls.Add(this.btnResourceMonitor);
            this.tabProcesses.Controls.Add(this.btnTaskManager);
            this.tabProcesses.Controls.Add(this.txtProcesses);
            this.tabProcesses.Location = new System.Drawing.Point(4, 22);
            this.tabProcesses.Name = "tabProcesses";
            this.tabProcesses.Padding = new System.Windows.Forms.Padding(3);
            this.tabProcesses.Size = new System.Drawing.Size(960, 571);
            this.tabProcesses.TabIndex = 1;
            this.tabProcesses.Text = "Processes";
            this.tabProcesses.UseVisualStyleBackColor = true;
            // 
            // btnResourceMonitor
            // 
            this.btnResourceMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResourceMonitor.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResourceMonitor.Image = ((System.Drawing.Image)(resources.GetObject("btnResourceMonitor.Image")));
            this.btnResourceMonitor.Location = new System.Drawing.Point(766, 42);
            this.btnResourceMonitor.Name = "btnResourceMonitor";
            this.btnResourceMonitor.Size = new System.Drawing.Size(188, 30);
            this.btnResourceMonitor.TabIndex = 2;
            this.btnResourceMonitor.Text = "Resource Monitor";
            this.btnResourceMonitor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnResourceMonitor.UseVisualStyleBackColor = true;
            this.btnResourceMonitor.Click += new System.EventHandler(this.btnResourceMonitor_Click);
            // 
            // btnTaskManager
            // 
            this.btnTaskManager.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTaskManager.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaskManager.Image = ((System.Drawing.Image)(resources.GetObject("btnTaskManager.Image")));
            this.btnTaskManager.Location = new System.Drawing.Point(766, 6);
            this.btnTaskManager.Name = "btnTaskManager";
            this.btnTaskManager.Size = new System.Drawing.Size(188, 30);
            this.btnTaskManager.TabIndex = 1;
            this.btnTaskManager.Text = "Task Manager";
            this.btnTaskManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTaskManager.UseVisualStyleBackColor = true;
            this.btnTaskManager.Click += new System.EventHandler(this.btnTaskManager_Click);
            // 
            // txtProcesses
            // 
            this.txtProcesses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProcesses.BackColor = System.Drawing.SystemColors.Window;
            this.txtProcesses.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcesses.Location = new System.Drawing.Point(6, 6);
            this.txtProcesses.MaxLength = 65565;
            this.txtProcesses.Multiline = true;
            this.txtProcesses.Name = "txtProcesses";
            this.txtProcesses.ReadOnly = true;
            this.txtProcesses.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtProcesses.Size = new System.Drawing.Size(754, 559);
            this.txtProcesses.TabIndex = 0;
            this.txtProcesses.Text = "Loading process data...";
            this.txtProcesses.WordWrap = false;
            // 
            // tabServices
            // 
            this.tabServices.Controls.Add(this.btnServices);
            this.tabServices.Controls.Add(this.txtServices);
            this.tabServices.Location = new System.Drawing.Point(4, 22);
            this.tabServices.Name = "tabServices";
            this.tabServices.Padding = new System.Windows.Forms.Padding(3);
            this.tabServices.Size = new System.Drawing.Size(960, 571);
            this.tabServices.TabIndex = 13;
            this.tabServices.Text = "Services";
            this.tabServices.UseVisualStyleBackColor = true;
            // 
            // btnServices
            // 
            this.btnServices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnServices.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServices.Image = ((System.Drawing.Image)(resources.GetObject("btnServices.Image")));
            this.btnServices.Location = new System.Drawing.Point(766, 6);
            this.btnServices.Name = "btnServices";
            this.btnServices.Size = new System.Drawing.Size(188, 30);
            this.btnServices.TabIndex = 2;
            this.btnServices.Text = "Services";
            this.btnServices.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnServices.UseVisualStyleBackColor = true;
            this.btnServices.Click += new System.EventHandler(this.btnServices_Click);
            // 
            // txtServices
            // 
            this.txtServices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServices.BackColor = System.Drawing.SystemColors.Window;
            this.txtServices.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServices.Location = new System.Drawing.Point(6, 6);
            this.txtServices.MaxLength = 65565;
            this.txtServices.Multiline = true;
            this.txtServices.Name = "txtServices";
            this.txtServices.ReadOnly = true;
            this.txtServices.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtServices.Size = new System.Drawing.Size(754, 559);
            this.txtServices.TabIndex = 1;
            this.txtServices.Text = "Loading services data...";
            this.txtServices.WordWrap = false;
            // 
            // tabRuntimes
            // 
            this.tabRuntimes.Controls.Add(this.txtRuntime);
            this.tabRuntimes.Location = new System.Drawing.Point(4, 22);
            this.tabRuntimes.Name = "tabRuntimes";
            this.tabRuntimes.Size = new System.Drawing.Size(960, 571);
            this.tabRuntimes.TabIndex = 4;
            this.tabRuntimes.Text = "Runtimes";
            this.tabRuntimes.UseVisualStyleBackColor = true;
            // 
            // txtRuntime
            // 
            this.txtRuntime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRuntime.BackColor = System.Drawing.SystemColors.Window;
            this.txtRuntime.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRuntime.Location = new System.Drawing.Point(6, 6);
            this.txtRuntime.MaxLength = 65565;
            this.txtRuntime.Multiline = true;
            this.txtRuntime.Name = "txtRuntime";
            this.txtRuntime.ReadOnly = true;
            this.txtRuntime.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRuntime.Size = new System.Drawing.Size(951, 559);
            this.txtRuntime.TabIndex = 0;
            this.txtRuntime.Text = "Loading runtime data...";
            this.txtRuntime.WordWrap = false;
            // 
            // tabEnvironmentVariables
            // 
            this.tabEnvironmentVariables.Controls.Add(this.txtEnvVariable);
            this.tabEnvironmentVariables.Location = new System.Drawing.Point(4, 22);
            this.tabEnvironmentVariables.Name = "tabEnvironmentVariables";
            this.tabEnvironmentVariables.Size = new System.Drawing.Size(960, 571);
            this.tabEnvironmentVariables.TabIndex = 5;
            this.tabEnvironmentVariables.Text = "Environment Variables";
            this.tabEnvironmentVariables.UseVisualStyleBackColor = true;
            // 
            // txtEnvVariable
            // 
            this.txtEnvVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEnvVariable.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnvVariable.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnvVariable.Location = new System.Drawing.Point(6, 6);
            this.txtEnvVariable.MaxLength = 65565;
            this.txtEnvVariable.Multiline = true;
            this.txtEnvVariable.Name = "txtEnvVariable";
            this.txtEnvVariable.ReadOnly = true;
            this.txtEnvVariable.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEnvVariable.Size = new System.Drawing.Size(951, 559);
            this.txtEnvVariable.TabIndex = 1;
            this.txtEnvVariable.Text = "Loading path data...";
            this.txtEnvVariable.WordWrap = false;
            // 
            // tabInstalledSoftware
            // 
            this.tabInstalledSoftware.Controls.Add(this.btnUninstall);
            this.tabInstalledSoftware.Controls.Add(this.txtPrograms);
            this.tabInstalledSoftware.Location = new System.Drawing.Point(4, 22);
            this.tabInstalledSoftware.Name = "tabInstalledSoftware";
            this.tabInstalledSoftware.Size = new System.Drawing.Size(960, 571);
            this.tabInstalledSoftware.TabIndex = 11;
            this.tabInstalledSoftware.Text = "Installed Software";
            this.tabInstalledSoftware.UseVisualStyleBackColor = true;
            // 
            // btnUninstall
            // 
            this.btnUninstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUninstall.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUninstall.Image = ((System.Drawing.Image)(resources.GetObject("btnUninstall.Image")));
            this.btnUninstall.Location = new System.Drawing.Point(766, 6);
            this.btnUninstall.Name = "btnUninstall";
            this.btnUninstall.Size = new System.Drawing.Size(188, 30);
            this.btnUninstall.TabIndex = 2;
            this.btnUninstall.Text = "Uninstall Programs";
            this.btnUninstall.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUninstall.UseVisualStyleBackColor = true;
            this.btnUninstall.Click += new System.EventHandler(this.btnUninstall_Click);
            // 
            // txtPrograms
            // 
            this.txtPrograms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrograms.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrograms.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrograms.Location = new System.Drawing.Point(6, 6);
            this.txtPrograms.Multiline = true;
            this.txtPrograms.Name = "txtPrograms";
            this.txtPrograms.ReadOnly = true;
            this.txtPrograms.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPrograms.Size = new System.Drawing.Size(754, 559);
            this.txtPrograms.TabIndex = 0;
            this.txtPrograms.Text = "Loading installed software data...";
            // 
            // tabHotFixes
            // 
            this.tabHotFixes.Controls.Add(this.btnWindowsUpdates);
            this.tabHotFixes.Controls.Add(this.txtHotFixes);
            this.tabHotFixes.Location = new System.Drawing.Point(4, 22);
            this.tabHotFixes.Name = "tabHotFixes";
            this.tabHotFixes.Size = new System.Drawing.Size(960, 571);
            this.tabHotFixes.TabIndex = 6;
            this.tabHotFixes.Text = "Hot Fixes";
            this.tabHotFixes.UseVisualStyleBackColor = true;
            // 
            // btnWindowsUpdates
            // 
            this.btnWindowsUpdates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWindowsUpdates.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWindowsUpdates.Image = ((System.Drawing.Image)(resources.GetObject("btnWindowsUpdates.Image")));
            this.btnWindowsUpdates.Location = new System.Drawing.Point(766, 6);
            this.btnWindowsUpdates.Name = "btnWindowsUpdates";
            this.btnWindowsUpdates.Size = new System.Drawing.Size(188, 30);
            this.btnWindowsUpdates.TabIndex = 5;
            this.btnWindowsUpdates.Text = "Windows Update";
            this.btnWindowsUpdates.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnWindowsUpdates.UseVisualStyleBackColor = true;
            this.btnWindowsUpdates.Click += new System.EventHandler(this.btnWinUpdate_Click);
            // 
            // txtHotFixes
            // 
            this.txtHotFixes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHotFixes.BackColor = System.Drawing.SystemColors.Window;
            this.txtHotFixes.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHotFixes.Location = new System.Drawing.Point(6, 6);
            this.txtHotFixes.Multiline = true;
            this.txtHotFixes.Name = "txtHotFixes";
            this.txtHotFixes.ReadOnly = true;
            this.txtHotFixes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtHotFixes.Size = new System.Drawing.Size(754, 559);
            this.txtHotFixes.TabIndex = 1;
            this.txtHotFixes.Text = "Loading hot fix data...";
            this.txtHotFixes.WordWrap = false;
            // 
            // tabTools
            // 
            this.tabTools.Controls.Add(this.btnServices1);
            this.tabTools.Controls.Add(this.btnRegedit);
            this.tabTools.Controls.Add(this.btnMobilityCenter);
            this.tabTools.Controls.Add(this.btnCredentialManager);
            this.tabTools.Controls.Add(this.label10);
            this.tabTools.Controls.Add(this.lnkLblOther);
            this.tabTools.Controls.Add(this.lnkLblKaspersky);
            this.tabTools.Controls.Add(this.lnkLblAvira);
            this.tabTools.Controls.Add(this.groupBox4);
            this.tabTools.Controls.Add(this.label9);
            this.tabTools.Controls.Add(this.btnBackupAndRestore);
            this.tabTools.Controls.Add(this.btnMasterControlPanel);
            this.tabTools.Controls.Add(this.groupBox3);
            this.tabTools.Controls.Add(this.label5);
            this.tabTools.Controls.Add(this.groupBox2);
            this.tabTools.Controls.Add(this.label4);
            this.tabTools.Controls.Add(this.groupBox1);
            this.tabTools.Controls.Add(this.label3);
            this.tabTools.Controls.Add(this.btnTroubleshooting);
            this.tabTools.Controls.Add(this.btnPerformDiag);
            this.tabTools.Controls.Add(this.btnGenSysReport);
            this.tabTools.Controls.Add(this.btnReliabilityMonitor);
            this.tabTools.Controls.Add(this.btnSystemMaintenance1);
            this.tabTools.Controls.Add(this.btnPSR);
            this.tabTools.Controls.Add(this.btnPerfMon);
            this.tabTools.Controls.Add(this.btnFolderOptions);
            this.tabTools.Controls.Add(this.btnResourceMon);
            this.tabTools.Controls.Add(this.btnTaskMgr);
            this.tabTools.Controls.Add(this.btnEventViewer);
            this.tabTools.Controls.Add(this.btnAboutWindows);
            this.tabTools.Controls.Add(this.btnSysConfig);
            this.tabTools.Controls.Add(this.btnSysInfo);
            this.tabTools.Controls.Add(this.btnRemoteDesktop);
            this.tabTools.Controls.Add(this.btnDeviceMgr);
            this.tabTools.Controls.Add(this.btnAdminTools);
            this.tabTools.Controls.Add(this.btnControlPanel);
            this.tabTools.Controls.Add(this.btnCmd);
            this.tabTools.Location = new System.Drawing.Point(4, 22);
            this.tabTools.Name = "tabTools";
            this.tabTools.Size = new System.Drawing.Size(960, 571);
            this.tabTools.TabIndex = 10;
            this.tabTools.Text = "Tools";
            this.tabTools.UseVisualStyleBackColor = true;
            // 
            // btnServices1
            // 
            this.btnServices1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServices1.Image = ((System.Drawing.Image)(resources.GetObject("btnServices1.Image")));
            this.btnServices1.Location = new System.Drawing.Point(229, 365);
            this.btnServices1.Name = "btnServices1";
            this.btnServices1.Size = new System.Drawing.Size(203, 30);
            this.btnServices1.TabIndex = 40;
            this.btnServices1.Text = "&Services";
            this.btnServices1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnServices1.UseVisualStyleBackColor = true;
            this.btnServices1.Click += new System.EventHandler(this.btnServices_Click);
            // 
            // btnRegedit
            // 
            this.btnRegedit.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.btnRegedit.Image = ((System.Drawing.Image)(resources.GetObject("btnRegedit.Image")));
            this.btnRegedit.Location = new System.Drawing.Point(474, 332);
            this.btnRegedit.Name = "btnRegedit";
            this.btnRegedit.Size = new System.Drawing.Size(189, 27);
            this.btnRegedit.TabIndex = 39;
            this.btnRegedit.Text = "&Regedit";
            this.btnRegedit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRegedit.UseVisualStyleBackColor = true;
            this.btnRegedit.Click += new System.EventHandler(this.btnRegedit_Click);
            // 
            // btnMobilityCenter
            // 
            this.btnMobilityCenter.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMobilityCenter.Image = ((System.Drawing.Image)(resources.GetObject("btnMobilityCenter.Image")));
            this.btnMobilityCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMobilityCenter.Location = new System.Drawing.Point(474, 297);
            this.btnMobilityCenter.Name = "btnMobilityCenter";
            this.btnMobilityCenter.Size = new System.Drawing.Size(189, 30);
            this.btnMobilityCenter.TabIndex = 38;
            this.btnMobilityCenter.Text = "&Mobility Center";
            this.btnMobilityCenter.UseVisualStyleBackColor = true;
            this.btnMobilityCenter.Click += new System.EventHandler(this.btnMobilityCenter_Click);
            // 
            // btnCredentialManager
            // 
            this.btnCredentialManager.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCredentialManager.Image = ((System.Drawing.Image)(resources.GetObject("btnCredentialManager.Image")));
            this.btnCredentialManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCredentialManager.Location = new System.Drawing.Point(229, 329);
            this.btnCredentialManager.Name = "btnCredentialManager";
            this.btnCredentialManager.Size = new System.Drawing.Size(203, 30);
            this.btnCredentialManager.TabIndex = 37;
            this.btnCredentialManager.Text = "&Credential Manager";
            this.btnCredentialManager.UseVisualStyleBackColor = true;
            this.btnCredentialManager.Click += new System.EventHandler(this.btnCredentialManager_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 431);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(396, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "You will need an internet connection to download an iso image from following link" +
    "s:";
            // 
            // lnkLblOther
            // 
            this.lnkLblOther.AutoSize = true;
            this.lnkLblOther.Location = new System.Drawing.Point(27, 500);
            this.lnkLblOther.Name = "lnkLblOther";
            this.lnkLblOther.Size = new System.Drawing.Size(99, 13);
            this.lnkLblOther.TabIndex = 35;
            this.lnkLblOther.TabStop = true;
            this.lnkLblOther.Text = "Search live AV disk";
            this.lnkLblOther.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblOther_LinkClicked);
            // 
            // lnkLblKaspersky
            // 
            this.lnkLblKaspersky.AutoSize = true;
            this.lnkLblKaspersky.Location = new System.Drawing.Point(27, 476);
            this.lnkLblKaspersky.Name = "lnkLblKaspersky";
            this.lnkLblKaspersky.Size = new System.Drawing.Size(120, 13);
            this.lnkLblKaspersky.TabIndex = 34;
            this.lnkLblKaspersky.TabStop = true;
            this.lnkLblKaspersky.Text = "Kaspersky Rescue Disk";
            this.lnkLblKaspersky.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Kaspersky_LinkClicked);
            // 
            // lnkLblAvira
            // 
            this.lnkLblAvira.AutoSize = true;
            this.lnkLblAvira.Location = new System.Drawing.Point(27, 452);
            this.lnkLblAvira.Name = "lnkLblAvira";
            this.lnkLblAvira.Size = new System.Drawing.Size(108, 13);
            this.lnkLblAvira.TabIndex = 33;
            this.lnkLblAvira.TabStop = true;
            this.lnkLblAvira.Text = "Avira Rescue System";
            this.lnkLblAvira.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblAvira_LinkClicked);
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(10, 426);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(430, 2);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 407);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(208, 16);
            this.label9.TabIndex = 31;
            this.label9.Text = "Antivirus and Rescue Disk";
            // 
            // btnBackupAndRestore
            // 
            this.btnBackupAndRestore.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.btnBackupAndRestore.Location = new System.Drawing.Point(475, 261);
            this.btnBackupAndRestore.Name = "btnBackupAndRestore";
            this.btnBackupAndRestore.Size = new System.Drawing.Size(187, 30);
            this.btnBackupAndRestore.TabIndex = 32;
            this.btnBackupAndRestore.Text = "&Backup and Restore";
            this.btnBackupAndRestore.UseVisualStyleBackColor = true;
            this.btnBackupAndRestore.Click += new System.EventHandler(this.btnBackupAndRestore_Click);
            // 
            // btnMasterControlPanel
            // 
            this.btnMasterControlPanel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMasterControlPanel.Location = new System.Drawing.Point(475, 189);
            this.btnMasterControlPanel.Name = "btnMasterControlPanel";
            this.btnMasterControlPanel.Size = new System.Drawing.Size(188, 30);
            this.btnMasterControlPanel.TabIndex = 31;
            this.btnMasterControlPanel.Text = "Master Control Panel";
            this.btnMasterControlPanel.UseVisualStyleBackColor = true;
            this.btnMasterControlPanel.Click += new System.EventHandler(this.btnMasterControlPanel_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(474, 33);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 2);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(472, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Other Tools";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(229, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(225, 2);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(228, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Admin Tools";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(9, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 2);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Troubleshooting";
            // 
            // btnTroubleshooting
            // 
            this.btnTroubleshooting.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTroubleshooting.Location = new System.Drawing.Point(9, 281);
            this.btnTroubleshooting.Name = "btnTroubleshooting";
            this.btnTroubleshooting.Size = new System.Drawing.Size(188, 30);
            this.btnTroubleshooting.TabIndex = 6;
            this.btnTroubleshooting.Text = "&Troubleshooting";
            this.btnTroubleshooting.UseVisualStyleBackColor = true;
            this.btnTroubleshooting.Click += new System.EventHandler(this.btnTroubleshooting_Click);
            // 
            // btnPerformDiag
            // 
            this.btnPerformDiag.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPerformDiag.Image = ((System.Drawing.Image)(resources.GetObject("btnPerformDiag.Image")));
            this.btnPerformDiag.Location = new System.Drawing.Point(9, 225);
            this.btnPerformDiag.Name = "btnPerformDiag";
            this.btnPerformDiag.Size = new System.Drawing.Size(188, 50);
            this.btnPerformDiag.TabIndex = 5;
            this.btnPerformDiag.Text = "Performance &Diagnostics";
            this.btnPerformDiag.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPerformDiag.UseVisualStyleBackColor = true;
            this.btnPerformDiag.Click += new System.EventHandler(this.btnPerformDiag_Click);
            // 
            // btnGenSysReport
            // 
            this.btnGenSysReport.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenSysReport.Image = ((System.Drawing.Image)(resources.GetObject("btnGenSysReport.Image")));
            this.btnGenSysReport.Location = new System.Drawing.Point(9, 169);
            this.btnGenSysReport.Name = "btnGenSysReport";
            this.btnGenSysReport.Size = new System.Drawing.Size(188, 50);
            this.btnGenSysReport.TabIndex = 4;
            this.btnGenSysReport.Text = "Generate System Re&port";
            this.btnGenSysReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenSysReport.UseVisualStyleBackColor = true;
            this.btnGenSysReport.Click += new System.EventHandler(this.btnSystemReport_Click);
            // 
            // btnReliabilityMonitor
            // 
            this.btnReliabilityMonitor.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReliabilityMonitor.Image = ((System.Drawing.Image)(resources.GetObject("btnReliabilityMonitor.Image")));
            this.btnReliabilityMonitor.Location = new System.Drawing.Point(9, 133);
            this.btnReliabilityMonitor.Name = "btnReliabilityMonitor";
            this.btnReliabilityMonitor.Size = new System.Drawing.Size(188, 30);
            this.btnReliabilityMonitor.TabIndex = 3;
            this.btnReliabilityMonitor.Text = "Re&liability Monitor";
            this.btnReliabilityMonitor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReliabilityMonitor.UseVisualStyleBackColor = true;
            this.btnReliabilityMonitor.Click += new System.EventHandler(this.btnRelbMonitor_Click);
            // 
            // btnSystemMaintenance1
            // 
            this.btnSystemMaintenance1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSystemMaintenance1.Image = ((System.Drawing.Image)(resources.GetObject("btnSystemMaintenance1.Image")));
            this.btnSystemMaintenance1.Location = new System.Drawing.Point(9, 97);
            this.btnSystemMaintenance1.Name = "btnSystemMaintenance1";
            this.btnSystemMaintenance1.Size = new System.Drawing.Size(188, 30);
            this.btnSystemMaintenance1.TabIndex = 2;
            this.btnSystemMaintenance1.Text = "System &Maintenance";
            this.btnSystemMaintenance1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSystemMaintenance1.UseVisualStyleBackColor = true;
            this.btnSystemMaintenance1.Click += new System.EventHandler(this.btnSystemMaintenance1_Click);
            // 
            // btnPSR
            // 
            this.btnPSR.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPSR.Image = ((System.Drawing.Image)(resources.GetObject("btnPSR.Image")));
            this.btnPSR.Location = new System.Drawing.Point(9, 41);
            this.btnPSR.Name = "btnPSR";
            this.btnPSR.Size = new System.Drawing.Size(188, 50);
            this.btnPSR.TabIndex = 1;
            this.btnPSR.Text = "Problem Steps &Recorder";
            this.btnPSR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPSR.UseVisualStyleBackColor = true;
            this.btnPSR.Click += new System.EventHandler(this.btnPSR_Click);
            // 
            // btnPerfMon
            // 
            this.btnPerfMon.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPerfMon.Image = ((System.Drawing.Image)(resources.GetObject("btnPerfMon.Image")));
            this.btnPerfMon.Location = new System.Drawing.Point(229, 293);
            this.btnPerfMon.Name = "btnPerfMon";
            this.btnPerfMon.Size = new System.Drawing.Size(203, 30);
            this.btnPerfMon.TabIndex = 15;
            this.btnPerfMon.Text = "&Performance Monitor";
            this.btnPerfMon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPerfMon.UseVisualStyleBackColor = true;
            this.btnPerfMon.Click += new System.EventHandler(this.btnPerfMon_Click);
            // 
            // btnFolderOptions
            // 
            this.btnFolderOptions.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFolderOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFolderOptions.Image")));
            this.btnFolderOptions.Location = new System.Drawing.Point(474, 77);
            this.btnFolderOptions.Name = "btnFolderOptions";
            this.btnFolderOptions.Size = new System.Drawing.Size(188, 30);
            this.btnFolderOptions.TabIndex = 18;
            this.btnFolderOptions.Text = "Folder Options";
            this.btnFolderOptions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFolderOptions.UseVisualStyleBackColor = true;
            this.btnFolderOptions.Click += new System.EventHandler(this.btnFolderOptions_Click);
            // 
            // btnResourceMon
            // 
            this.btnResourceMon.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResourceMon.Image = ((System.Drawing.Image)(resources.GetObject("btnResourceMon.Image")));
            this.btnResourceMon.Location = new System.Drawing.Point(229, 257);
            this.btnResourceMon.Name = "btnResourceMon";
            this.btnResourceMon.Size = new System.Drawing.Size(203, 30);
            this.btnResourceMon.TabIndex = 14;
            this.btnResourceMon.Text = "Resource M&onitor";
            this.btnResourceMon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnResourceMon.UseVisualStyleBackColor = true;
            this.btnResourceMon.Click += new System.EventHandler(this.btnResourceMonitor_Click);
            // 
            // btnTaskMgr
            // 
            this.btnTaskMgr.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaskMgr.Image = ((System.Drawing.Image)(resources.GetObject("btnTaskMgr.Image")));
            this.btnTaskMgr.Location = new System.Drawing.Point(229, 221);
            this.btnTaskMgr.Name = "btnTaskMgr";
            this.btnTaskMgr.Size = new System.Drawing.Size(203, 30);
            this.btnTaskMgr.TabIndex = 13;
            this.btnTaskMgr.Text = "Tas&k Manager";
            this.btnTaskMgr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTaskMgr.UseVisualStyleBackColor = true;
            this.btnTaskMgr.Click += new System.EventHandler(this.btnTaskManager_Click);
            // 
            // btnEventViewer
            // 
            this.btnEventViewer.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEventViewer.Image = ((System.Drawing.Image)(resources.GetObject("btnEventViewer.Image")));
            this.btnEventViewer.Location = new System.Drawing.Point(229, 113);
            this.btnEventViewer.Name = "btnEventViewer";
            this.btnEventViewer.Size = new System.Drawing.Size(203, 30);
            this.btnEventViewer.TabIndex = 10;
            this.btnEventViewer.Text = "&Event Viewer";
            this.btnEventViewer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEventViewer.UseVisualStyleBackColor = true;
            this.btnEventViewer.Click += new System.EventHandler(this.btnEventViewer_Click);
            // 
            // btnAboutWindows
            // 
            this.btnAboutWindows.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAboutWindows.Image = ((System.Drawing.Image)(resources.GetObject("btnAboutWindows.Image")));
            this.btnAboutWindows.Location = new System.Drawing.Point(475, 41);
            this.btnAboutWindows.Name = "btnAboutWindows";
            this.btnAboutWindows.Size = new System.Drawing.Size(188, 30);
            this.btnAboutWindows.TabIndex = 20;
            this.btnAboutWindows.Text = "About &Windows";
            this.btnAboutWindows.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAboutWindows.UseVisualStyleBackColor = true;
            this.btnAboutWindows.Click += new System.EventHandler(this.btnAboutWindows_Click);
            // 
            // btnSysConfig
            // 
            this.btnSysConfig.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSysConfig.Location = new System.Drawing.Point(229, 185);
            this.btnSysConfig.Name = "btnSysConfig";
            this.btnSysConfig.Size = new System.Drawing.Size(203, 30);
            this.btnSysConfig.TabIndex = 12;
            this.btnSysConfig.Text = "System Con&figuration";
            this.btnSysConfig.UseVisualStyleBackColor = true;
            this.btnSysConfig.Click += new System.EventHandler(this.btnSysConfig_Click);
            // 
            // btnSysInfo
            // 
            this.btnSysInfo.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSysInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnSysInfo.Image")));
            this.btnSysInfo.Location = new System.Drawing.Point(229, 149);
            this.btnSysInfo.Name = "btnSysInfo";
            this.btnSysInfo.Size = new System.Drawing.Size(203, 30);
            this.btnSysInfo.TabIndex = 11;
            this.btnSysInfo.Text = "System &Information";
            this.btnSysInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSysInfo.UseVisualStyleBackColor = true;
            this.btnSysInfo.Click += new System.EventHandler(this.btnOpenSysInfo_Click);
            // 
            // btnRemoteDesktop
            // 
            this.btnRemoteDesktop.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoteDesktop.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoteDesktop.Image")));
            this.btnRemoteDesktop.Location = new System.Drawing.Point(474, 113);
            this.btnRemoteDesktop.Name = "btnRemoteDesktop";
            this.btnRemoteDesktop.Size = new System.Drawing.Size(188, 30);
            this.btnRemoteDesktop.TabIndex = 19;
            this.btnRemoteDesktop.Text = "Remote Desktop";
            this.btnRemoteDesktop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRemoteDesktop.UseVisualStyleBackColor = true;
            this.btnRemoteDesktop.Click += new System.EventHandler(this.btnRemoteDesktop_Click);
            // 
            // btnDeviceMgr
            // 
            this.btnDeviceMgr.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeviceMgr.Location = new System.Drawing.Point(229, 77);
            this.btnDeviceMgr.Name = "btnDeviceMgr";
            this.btnDeviceMgr.Size = new System.Drawing.Size(203, 30);
            this.btnDeviceMgr.TabIndex = 9;
            this.btnDeviceMgr.Text = "Device Ma&nager";
            this.btnDeviceMgr.UseVisualStyleBackColor = true;
            this.btnDeviceMgr.Click += new System.EventHandler(this.btnDeviceMgr_Click);
            // 
            // btnAdminTools
            // 
            this.btnAdminTools.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdminTools.Image = ((System.Drawing.Image)(resources.GetObject("btnAdminTools.Image")));
            this.btnAdminTools.Location = new System.Drawing.Point(229, 41);
            this.btnAdminTools.Name = "btnAdminTools";
            this.btnAdminTools.Size = new System.Drawing.Size(203, 30);
            this.btnAdminTools.TabIndex = 8;
            this.btnAdminTools.Text = "&Administrative Tools";
            this.btnAdminTools.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdminTools.UseVisualStyleBackColor = true;
            this.btnAdminTools.Click += new System.EventHandler(this.btnAdminTools_Click);
            // 
            // btnControlPanel
            // 
            this.btnControlPanel.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnControlPanel.Image = ((System.Drawing.Image)(resources.GetObject("btnControlPanel.Image")));
            this.btnControlPanel.Location = new System.Drawing.Point(475, 225);
            this.btnControlPanel.Name = "btnControlPanel";
            this.btnControlPanel.Size = new System.Drawing.Size(188, 30);
            this.btnControlPanel.TabIndex = 17;
            this.btnControlPanel.Text = "&Control Panel";
            this.btnControlPanel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnControlPanel.UseVisualStyleBackColor = true;
            this.btnControlPanel.Click += new System.EventHandler(this.btnControlPanel_Click);
            // 
            // btnCmd
            // 
            this.btnCmd.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCmd.Image = ((System.Drawing.Image)(resources.GetObject("btnCmd.Image")));
            this.btnCmd.Location = new System.Drawing.Point(475, 149);
            this.btnCmd.Name = "btnCmd";
            this.btnCmd.Size = new System.Drawing.Size(188, 30);
            this.btnCmd.TabIndex = 21;
            this.btnCmd.Text = "Command Prompt";
            this.btnCmd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCmd.UseVisualStyleBackColor = true;
            this.btnCmd.Click += new System.EventHandler(this.btnCmd_Click);
            // 
            // tabAbout
            // 
            this.tabAbout.Controls.Add(this.pictureBox1);
            this.tabAbout.Controls.Add(this.lblVersion);
            this.tabAbout.Controls.Add(this.tabControl3);
            this.tabAbout.Controls.Add(this.label2);
            this.tabAbout.Controls.Add(this.label1);
            this.tabAbout.Location = new System.Drawing.Point(4, 22);
            this.tabAbout.Name = "tabAbout";
            this.tabAbout.Size = new System.Drawing.Size(960, 571);
            this.tabAbout.TabIndex = 9;
            this.tabAbout.Text = "About";
            this.tabAbout.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(458, 29);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(40, 16);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "v0.4";
            // 
            // tabControl3
            // 
            this.tabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl3.Controls.Add(this.tabPage15);
            this.tabControl3.Controls.Add(this.tabPage16);
            this.tabControl3.Location = new System.Drawing.Point(6, 215);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(948, 350);
            this.tabControl3.TabIndex = 2;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.txtDebug);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(940, 324);
            this.tabPage15.TabIndex = 0;
            this.tabPage15.Text = "Debug info";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // txtDebug
            // 
            this.txtDebug.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDebug.BackColor = System.Drawing.SystemColors.Window;
            this.txtDebug.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebug.Location = new System.Drawing.Point(6, 6);
            this.txtDebug.Multiline = true;
            this.txtDebug.Name = "txtDebug";
            this.txtDebug.ReadOnly = true;
            this.txtDebug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDebug.Size = new System.Drawing.Size(928, 312);
            this.txtDebug.TabIndex = 0;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.txtError);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(940, 324);
            this.tabPage16.TabIndex = 1;
            this.tabPage16.Text = "Errors";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // txtError
            // 
            this.txtError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtError.BackColor = System.Drawing.SystemColors.Window;
            this.txtError.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtError.Location = new System.Drawing.Point(6, 6);
            this.txtError.Multiline = true;
            this.txtError.Name = "txtError";
            this.txtError.ReadOnly = true;
            this.txtError.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtError.Size = new System.Drawing.Size(928, 312);
            this.txtError.TabIndex = 0;
            this.txtError.WordWrap = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "© 2016 Bhushan N Kalse.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(54, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(398, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "System Diagnostics and Information Tool";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(235, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "to";
            // 
            // bgwNetworkPorts
            // 
            this.bgwNetworkPorts.WorkerSupportsCancellation = true;
            this.bgwNetworkPorts.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwNetworkPorts_DoWork);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStatus.Location = new System.Drawing.Point(19, 620);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(660, 17);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Status: Ready";
            // 
            // btnSaveAll
            // 
            this.btnSaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAll.Location = new System.Drawing.Point(871, 615);
            this.btnSaveAll.Name = "btnSaveAll";
            this.btnSaveAll.Size = new System.Drawing.Size(109, 23);
            this.btnSaveAll.TabIndex = 2;
            this.btnSaveAll.Text = "&Save All Data...";
            this.btnSaveAll.UseVisualStyleBackColor = true;
            this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
            // 
            // btnCopyToCLipboard
            // 
            this.btnCopyToCLipboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyToCLipboard.Location = new System.Drawing.Point(743, 615);
            this.btnCopyToCLipboard.Name = "btnCopyToCLipboard";
            this.btnCopyToCLipboard.Size = new System.Drawing.Size(122, 23);
            this.btnCopyToCLipboard.TabIndex = 1;
            this.btnCopyToCLipboard.Text = "&Copy To Clipboard";
            this.btnCopyToCLipboard.UseVisualStyleBackColor = true;
            this.btnCopyToCLipboard.Click += new System.EventHandler(this.btnCopyToCLipboard_Click);
            // 
            // bgwOS
            // 
            this.bgwOS.WorkerSupportsCancellation = true;
            this.bgwOS.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwOS_DoWork);
            // 
            // timerPing
            // 
            this.timerPing.Enabled = true;
            this.timerPing.Interval = 30000;
            this.timerPing.Tick += new System.EventHandler(this.timerPing_Tick);
            // 
            // bgwHDD
            // 
            this.bgwHDD.WorkerSupportsCancellation = true;
            this.bgwHDD.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwHDD_DoWork);
            // 
            // bgwNetwork
            // 
            this.bgwNetwork.WorkerSupportsCancellation = true;
            this.bgwNetwork.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwNetwork_DoWork);
            // 
            // bgwInstalledPrograms
            // 
            this.bgwInstalledPrograms.WorkerReportsProgress = true;
            this.bgwInstalledPrograms.WorkerSupportsCancellation = true;
            this.bgwInstalledPrograms.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwInstalledPrograms_DoWork);
            // 
            // bgwProcessInfo
            // 
            this.bgwProcessInfo.WorkerReportsProgress = true;
            this.bgwProcessInfo.WorkerSupportsCancellation = true;
            this.bgwProcessInfo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwProcessInfo_DoWork);
            // 
            // bgwRuntime
            // 
            this.bgwRuntime.WorkerReportsProgress = true;
            this.bgwRuntime.WorkerSupportsCancellation = true;
            this.bgwRuntime.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwRuntime_DoWork);
            // 
            // bgwEnvironmentVars
            // 
            this.bgwEnvironmentVars.WorkerSupportsCancellation = true;
            this.bgwEnvironmentVars.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwEnvironmentVars_DoWork);
            // 
            // bgwHotFixes
            // 
            this.bgwHotFixes.WorkerSupportsCancellation = true;
            this.bgwHotFixes.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwHotFixes_DoWork);
            // 
            // bgwNetworkConnections
            // 
            this.bgwNetworkConnections.WorkerReportsProgress = true;
            this.bgwNetworkConnections.WorkerSupportsCancellation = true;
            this.bgwNetworkConnections.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwNetworkConnections_DoWork);
            // 
            // bgwStartupItems
            // 
            this.bgwStartupItems.WorkerReportsProgress = true;
            this.bgwStartupItems.WorkerSupportsCancellation = true;
            this.bgwStartupItems.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwStartupItems_DoWork);
            // 
            // ipControlTo
            // 
            this.ipControlTo.EnableSubnetChange = false;
            this.ipControlTo.IP = "...";
            this.ipControlTo.Location = new System.Drawing.Point(257, 10);
            this.ipControlTo.Name = "ipControlTo";
            this.ipControlTo.Size = new System.Drawing.Size(166, 25);
            this.ipControlTo.TabIndex = 8;
            // 
            // ipControlFrom
            // 
            this.ipControlFrom.EnableSubnetChange = true;
            this.ipControlFrom.IP = "...";
            this.ipControlFrom.Location = new System.Drawing.Point(63, 10);
            this.ipControlFrom.Name = "ipControlFrom";
            this.ipControlFrom.Size = new System.Drawing.Size(166, 25);
            this.ipControlFrom.TabIndex = 7;
            // 
            // bgwServices
            // 
            this.bgwServices.WorkerSupportsCancellation = true;
            this.bgwServices.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwServices_DoWork);
            // 
            // tabDevices
            // 
            this.tabDevices.Location = new System.Drawing.Point(4, 22);
            this.tabDevices.Name = "tabDevices";
            this.tabDevices.Padding = new System.Windows.Forms.Padding(3);
            this.tabDevices.Size = new System.Drawing.Size(960, 571);
            this.tabDevices.TabIndex = 14;
            this.tabDevices.Text = "tabPage5";
            this.tabDevices.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 643);
            this.Controls.Add(this.btnCopyToCLipboard);
            this.Controls.Add(this.btnSaveAll);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 650);
            this.Name = "MainForm";
            this.Text = "System Diagnostics and Information Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabSystem.ResumeLayout(false);
            this.tabSystem.PerformLayout();
            this.tabStartupItems.ResumeLayout(false);
            this.tabStartupItems.PerformLayout();
            this.tabHDD.ResumeLayout(false);
            this.tabHDD.PerformLayout();
            this.tabNetwork.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabProcesses.ResumeLayout(false);
            this.tabProcesses.PerformLayout();
            this.tabServices.ResumeLayout(false);
            this.tabServices.PerformLayout();
            this.tabRuntimes.ResumeLayout(false);
            this.tabRuntimes.PerformLayout();
            this.tabEnvironmentVariables.ResumeLayout(false);
            this.tabEnvironmentVariables.PerformLayout();
            this.tabInstalledSoftware.ResumeLayout(false);
            this.tabInstalledSoftware.PerformLayout();
            this.tabHotFixes.ResumeLayout(false);
            this.tabHotFixes.PerformLayout();
            this.tabTools.ResumeLayout(false);
            this.tabTools.PerformLayout();
            this.tabAbout.ResumeLayout(false);
            this.tabAbout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl3.ResumeLayout(false);
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabProcesses;
        private System.Windows.Forms.TabPage tabSystem;
        private System.ComponentModel.BackgroundWorker bgwNetworkPorts;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TabPage tabHDD;
        private System.Windows.Forms.TabPage tabNetwork;
        private System.Windows.Forms.TabPage tabRuntimes;
        private System.Windows.Forms.TabPage tabEnvironmentVariables;
        private System.Windows.Forms.TabPage tabHotFixes;
        private System.Windows.Forms.TabPage tabAbout;
        private System.Windows.Forms.TextBox txtSystem;
        private System.Windows.Forms.Button btnSaveAll;
        private System.Windows.Forms.Button btnCopyToCLipboard;
        private System.Windows.Forms.TabPage tabTools;
        private System.Windows.Forms.Button btnResourceMon;
        private System.Windows.Forms.Button btnTaskMgr;
        private System.Windows.Forms.Button btnEventViewer;
        private System.Windows.Forms.Button btnAboutWindows;
        private System.Windows.Forms.Button btnSysConfig;
        private System.Windows.Forms.Button btnSysInfo;
        private System.Windows.Forms.Button btnRemoteDesktop;
        private System.Windows.Forms.Button btnDeviceMgr;
        private System.Windows.Forms.Button btnAdminTools;
        private System.Windows.Forms.Button btnControlPanel;
        private System.Windows.Forms.Button btnCmd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabInstalledSoftware;
        private System.ComponentModel.BackgroundWorker bgwOS;
        private System.Windows.Forms.Button btnOpenSysInfo;
        private System.Windows.Forms.Button btnOpenFirewall;
        private System.Windows.Forms.Button btnOpenSystem;
        private System.Windows.Forms.Label lblInternet;
        private System.Windows.Forms.Timer timerPing;
        private System.Windows.Forms.TextBox txtHDD;
        private System.ComponentModel.BackgroundWorker bgwHDD;
        private System.Windows.Forms.Button btnDiskDefrag;
        private System.Windows.Forms.Button btnDiskCleanup;
        private System.ComponentModel.BackgroundWorker bgwNetwork;
        private System.ComponentModel.BackgroundWorker bgwInstalledPrograms;
        private System.Windows.Forms.TextBox txtPrograms;
        private System.Windows.Forms.Button btnWinUpdate;
        private System.Windows.Forms.Button btnPerfMon;
        private System.Windows.Forms.Button btnFolderOptions;
        private System.Windows.Forms.TextBox txtProcesses;
        private System.ComponentModel.BackgroundWorker bgwProcessInfo;
        private System.Windows.Forms.TextBox txtDebug;
        private System.Windows.Forms.Button btnResourceMonitor;
        private System.Windows.Forms.Button btnTaskManager;
        private System.Windows.Forms.Button btnRelbMonitor;
        private System.Windows.Forms.Button btnSystemReport;
        private System.Windows.Forms.Button btnGenSysReport;
        private System.Windows.Forms.Button btnReliabilityMonitor;
        private System.Windows.Forms.Button btnSystemMaintenance1;
        private System.Windows.Forms.Button btnPSR;
        private System.Windows.Forms.Button btnPerformDiag;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.Button btnTroubleshooting;
        private System.Windows.Forms.TextBox txtError;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRuntime;
        private System.ComponentModel.BackgroundWorker bgwRuntime;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TextBox txtEnvVariable;
        private System.ComponentModel.BackgroundWorker bgwEnvironmentVars;
        private System.Windows.Forms.Button btnMasterControlPanel;
        private System.Windows.Forms.TextBox txtHotFixes;
        private System.ComponentModel.BackgroundWorker bgwHotFixes;
        private System.Windows.Forms.Button btnWindowsUpdates;
        private System.Windows.Forms.Button btnUninstall;
        private IPControl ipControlFrom;
        private System.Windows.Forms.Label label8;
        private IPControl ipControlTo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.LinkLabel lnkLblAvira;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnBackupAndRestore;
        private System.Windows.Forms.LinkLabel lnkLblKaspersky;
        private System.Windows.Forms.LinkLabel lnkLblOther;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnOpenHostsFile;
        private System.Windows.Forms.Button btnIpConfig;
        private System.Windows.Forms.TextBox txtPingHost;
        private System.Windows.Forms.Button btnPing;
        private System.Windows.Forms.Button btnNWCEnter;
        private System.Windows.Forms.TextBox txtNetInterfaces;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtNetPorts;
        private System.Windows.Forms.Button btnRefreshNwOpenPorts;
        private System.Windows.Forms.TabPage tabPage3;
        private System.ComponentModel.BackgroundWorker bgwNetworkConnections;
        private System.Windows.Forms.TextBox txtNetConnections;
        private System.Windows.Forms.Button btnCredentialManager;
        private System.Windows.Forms.Button btnMobilityCenter;
        private System.Windows.Forms.TabPage tabStartupItems;
        private System.Windows.Forms.Button btnSysConfig1;
        private System.Windows.Forms.TextBox txtStartupItems;
        private System.ComponentModel.BackgroundWorker bgwStartupItems;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPathPing;
        private System.Windows.Forms.Button btnRegedit;
        private System.Windows.Forms.TabPage tabServices;
        private System.Windows.Forms.Button btnServices;
        private System.Windows.Forms.TextBox txtServices;
        private System.Windows.Forms.Button btnServices1;
        private System.ComponentModel.BackgroundWorker bgwServices;
        private System.Windows.Forms.TabPage tabDevices;
    }
}

