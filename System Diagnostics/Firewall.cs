﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace System_Diagnostics
{
    public class FirewallInfo
    {
        public static string GetFilrewallInfo()
        {

            try
            {
                using (Process p = new Process())
                {                    
                    ProcessStartInfo ps = new ProcessStartInfo();
                    ps.Arguments = "advfirewall show allprofiles";
                    ps.FileName = "netsh";
                    ps.UseShellExecute = false;
                    ps.WindowStyle = ProcessWindowStyle.Hidden;
                    ps.RedirectStandardInput = true;
                    ps.RedirectStandardOutput = true;
                    ps.RedirectStandardError = true;

                    p.StartInfo = ps;
                    p.Start();

                    StreamReader stdOutput = p.StandardOutput;
                    StreamReader stdError = p.StandardError;

                    string content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                    string exitStatus = p.ExitCode.ToString();

                    content = content.Replace("Ok.", "");

                    if (exitStatus != "0")
                    {
                        // Command Errored. Handle Here If Need Be
                    }

                    return content;
                    
                }
            }
            catch (Exception ex)
            {
                return "** Unknown **";
            }                       
        }
    }
}
