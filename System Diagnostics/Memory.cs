﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

namespace System_Diagnostics
{
    public class Memory
    {
        public static string GetTotalMemory()
        {
            return Utility.FormatBytes(new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory);
        }

        public static string GetAvailableMemory()
        {
            return Utility.FormatBytes(new Microsoft.VisualBasic.Devices.ComputerInfo().AvailablePhysicalMemory);
        }
    }
}
