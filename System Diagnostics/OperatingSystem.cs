﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Management;
using Microsoft.Win32;
using System.IO;

namespace System_Diagnostics
{
    public class OperatingSystem 
    {
        public static OperatingSystemInfo GetOperatingSystemInfo()
        {
            OperatingSystemInfo osInfo = new OperatingSystemInfo();
            
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");                

                foreach (ManagementObject queryObj in searcher.Get())
                {

                    osInfo.BuildNumber                  = queryObj["BuildNumber"].ToString();
                    osInfo.Caption                      = queryObj["Caption"].ToString();
                    osInfo.CountryCode                  =  Int32.Parse(queryObj["CountryCode"].ToString());
                    osInfo.CurrentTimeZone              = Int32.Parse(queryObj["CurrentTimeZone"].ToString());
                    osInfo.Description                  = queryObj["Description"].ToString();
                    osInfo.FreePhysicalMemory           = Utility.FormatBytes((ulong)Int64.Parse(queryObj["FreePhysicalMemory"].ToString()));
                    osInfo.FreeSpaceInPagingFiles       = Utility.FormatBytes((ulong)Int64.Parse(queryObj["FreeSpaceInPagingFiles"].ToString()));
                    osInfo.FreeVirtualMemory            = Utility.FormatBytes((ulong)Int64.Parse(queryObj["FreeVirtualMemory"].ToString()));

                    var id = ManagementDateTimeConverter.ToDateTime(queryObj["InstallDate"].ToString());
                    osInfo.InstallDate                  = id.ToShortDateString() + " " + id.ToShortTimeString(); ;

                    var lastBootUp = ManagementDateTimeConverter.ToDateTime(queryObj["LastBootUpTime"].ToString());
                    osInfo.LastBootUpTime               = lastBootUp.ToShortDateString() + " " + lastBootUp.ToShortTimeString();

                    var dt = ManagementDateTimeConverter.ToDateTime(queryObj["LocalDateTime"].ToString());
                    osInfo.LocalDateTime                = dt.ToShortDateString() + " " + dt.ToShortTimeString();                    
                    osInfo.MaxProcessMemorySize         = Utility.FormatBytes((ulong)Int64.Parse(queryObj["MaxProcessMemorySize"].ToString()));                    
                    osInfo.NumberOfProcesses            = Int32.Parse(queryObj["NumberOfProcesses"].ToString());

                    // following line clocks up this method on some machines.
                   // osInfo.NumberOfUsers                = Int32.Parse(queryObj["NumberOfUsers"].ToString());

                    osInfo.Organization                 = queryObj["Organization"].ToString();
                    osInfo.OSArchitecture               = queryObj["OSArchitecture"].ToString();
                    osInfo.RegisteredUser               = queryObj["RegisteredUser"].ToString();
                    osInfo.ServicePackMajorVersion      = Int32.Parse(queryObj["ServicePackMajorVersion"].ToString());
                    osInfo.ServicePackMinorVersion      = Int32.Parse(queryObj["ServicePackMinorVersion"].ToString());
                    osInfo.SystemDrive                  = queryObj["SystemDrive"].ToString();
                    osInfo.TotalVirtualMemorySize       = Utility.FormatBytes((ulong)Int64.Parse(queryObj["TotalVirtualMemorySize"].ToString()));
                    osInfo.TotalVisibleMemorySize       = Utility.FormatBytes((ulong)Int64.Parse(queryObj["TotalVisibleMemorySize"].ToString()));
                    osInfo.Version                      = queryObj["Version"].ToString();
                    osInfo.WindowsDirectory             = queryObj["WindowsDirectory"].ToString();
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return osInfo;
        }

        public static List<StartupItem> GetStartupItems()
        {
            List<StartupItem> startupItems = new List<StartupItem>();

            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Microsoft\\Windows\\CurrentVersion\\Run"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));

            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));

            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\BootExecute"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Microsoft\\Windows\\CurrentVersion\\Winlogon\\UserInit"));
            startupItems.AddRange(GetRegistryEntries(Registry.LocalMachine, "Software\\Microsoft\\Windows\\CurrentVersion\\Winlogon\\Shell"));

            startupItems.AddRange(GetRegistryEntries(Registry.CurrentUser, "Software\\Microsoft\\Windows\\CurrentVersion\\Run"));
            startupItems.AddRange(GetRegistryEntries(Registry.CurrentUser, "Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
            startupItems.AddRange(GetRegistryEntries(Registry.CurrentUser, "Software\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));


            DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Startup));
            FileInfo[] links = di.GetFiles("*.lnk");
            foreach (FileInfo fi in links)
            {
                startupItems.Add(new StartupItem {Command = fi.FullName });    
            }
            
            return startupItems;
        }

        private static List<StartupItem> GetRegistryEntries(RegistryKey registryKey, string path)
        {
            List<StartupItem> startupItems = new List<StartupItem>();
            RegistryKey key;
            key = registryKey.OpenSubKey(path, false);

            if (key != null)
            {
                foreach (string name in key.GetValueNames())
                {
                    string command = key.GetValue(name) as string;

                    if (string.IsNullOrEmpty(command) == true)
                        continue;

                    startupItems.Add(new StartupItem { Name = name, Command = command });
                }

                key.Close();
            }

            return startupItems;
        }

        
    }

    public class StartupItem
    {
        public string Name { get; set; }
        public string Command { get; set; }
    }

    public class OperatingSystemInfo
    {
        public string BuildNumber { get; set; }
        public string Caption { get; set; }
        public int CountryCode { get; set; }
        public int CurrentTimeZone { get; set; }
        public string Description { get; set; }
        public string FreePhysicalMemory { get; set; }
        public string FreeSpaceInPagingFiles { get; set; }
        public string FreeVirtualMemory { get; set; }
        public string InstallDate { get; set; }
        public string LastBootUpTime { get; set; }
        public string LocalDateTime { get; set; }
        public string MaxProcessMemorySize { get; set; }
        public string Organization { get; set; }
        public string OSArchitecture { get; set; }
        public string RegisteredUser { get; set; }
        public int ServicePackMajorVersion { get; set; }
        public int ServicePackMinorVersion { get; set; }
        public string SystemDrive { get; set; }
        public string TotalVirtualMemorySize { get; set; }
        public string TotalVisibleMemorySize { get; set; }
        public string Version { get; set; }
        public string WindowsDirectory { get; set; }
        public int NumberOfProcesses { get; set; }
        public int NumberOfUsers { get; set; }

        public OperatingSystemInfo()
        { 
        }
    }
}
