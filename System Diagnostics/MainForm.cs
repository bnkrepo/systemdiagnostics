﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace System_Diagnostics
{
    public partial class MainForm : Form
    {
        string _ip = "";
        bool _bInternetConnected = false;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            timerPing.Start();
            // force
            timerPing_Tick(null, null);

            CollectSystemData();

            lblVersion.Text = "v" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private void CollectSystemData()
        {
            bgwStartupItems.RunWorkerAsync();
            bgwNetworkPorts.RunWorkerAsync();
            bgwOS.RunWorkerAsync();
            bgwHDD.RunWorkerAsync();
            bgwNetwork.RunWorkerAsync();
            bgwNetworkConnections.RunWorkerAsync();
            bgwProcessInfo.RunWorkerAsync();
            bgwRuntime.RunWorkerAsync();
            bgwInstalledPrograms.RunWorkerAsync();
            bgwEnvironmentVars.RunWorkerAsync();
            bgwHotFixes.RunWorkerAsync();
            bgwServices.RunWorkerAsync();
        }

        private void bgwStartupItems_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting startup items information...");
            List<StartupItem> startupItems = OperatingSystem.GetStartupItems();

            this.txtStartupItems.Invoke((Action)delegate
            {
                StringBuilder sb = new StringBuilder();

                #region Netstat

                sb.AppendLine(String.Format("{0,-30 } {1, -50} ", "Name", "Command"));
                sb.AppendLine("----------------------------------------------------------------------------------------------------");

                foreach (var item in startupItems)
                {
                    if (string.IsNullOrEmpty(item.Name) == false)
                        sb.AppendLine(String.Format("{0,-30 } {1, -50}", item.Name, item.Command));
                    else
                        sb.AppendLine(String.Format("{0,-30 }", item.Command));
                }

                txtStartupItems.Text = sb.ToString();
                #endregion
            });
            
            startupItems.Clear();

            SetStatus(" ");
            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Startup items consumed : {0}", strTime));
        } 

        private void bgwNetworkPorts_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            EnableControl(btnRefreshNwOpenPorts, false);

            SetStatus("Collecting netstat information...");
            List<Port> ports = Network.GetNetStatPorts();                

            this.txtNetPorts.Invoke((Action)delegate
            {
                StringBuilder sb = new StringBuilder();

                #region Netstat

                sb.AppendLine(String.Format("{0,-10 } {1, -50} {2, -20} {3, -20}", "PID", "Process Name", "Port Number", "Protocol"));
                sb.AppendLine("------------------------------------------------------------------------------------------------------------------");

                foreach (var port in ports)
                {
                    sb.AppendLine(String.Format("{0,-10 } {1, -50} {2, -20} {3, -20}", port.Pid, port.ProcessName, port.PortNumber, port.Protocol));
                }

                sb.AppendLine("------------------------------------------------------------------------------------------------------------------");
                sb.AppendLine("Total " + ports.Count.ToString() + " ports in use found.");

                txtNetPorts.Text = sb.ToString();
                #endregion
            });

            ports.Clear();
            EnableControl(btnRefreshNwOpenPorts);
            SetStatus(" ");
            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Netstat information consumed : {0}", strTime));
        }
       
        private void bgwOS_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting OS information...");
            OperatingSystemInfo osInfo = OperatingSystem.GetOperatingSystemInfo();
            
            SetStatus("Collecting antivirus information...");
            AntivirusInfo avInfo = Antivirus.GetAVInfo();
            
            SetStatus("Collecting CPU information...");
            CPU cpu = CPUInfo.GetCPUInfo();

            this.txtSystem.Invoke((Action)delegate
            {
                string strFormat = "{0, -20} {1, -30}";
                string strFormat1 = "{0, -30} {1, -30}";
                string strLine = "----------------------------------------------------------------------------------------";
                StringBuilder sb = new StringBuilder();

                #region System

                // System
                sb.AppendLine("System Information");
                sb.AppendLine(strLine);
                sb.AppendLine(String.Format(strFormat, "Machine Name: ", Environment.MachineName));
                sb.AppendLine(String.Format(strFormat, "User Name: ", Environment.UserName));
                sb.AppendLine(String.Format(strFormat, "Domain Name: ", Environment.UserDomainName));
                sb.AppendLine(" ");

                #endregion 

                #region Operating System

                //OS                
                sb.AppendLine("Operation System");
                sb.AppendLine(strLine);

                sb.AppendLine(String.Format(strFormat1, "Caption: ", osInfo.Caption));
                sb.AppendLine(String.Format(strFormat1, "Description: ", osInfo.Description));
                sb.AppendLine(String.Format(strFormat1, "Version: ", osInfo.Version));
                sb.AppendLine(String.Format(strFormat1, "Service Pack: ", osInfo.ServicePackMajorVersion + "." + osInfo.ServicePackMinorVersion));
                sb.AppendLine(String.Format(strFormat1, "Build :", osInfo.BuildNumber));

                sb.AppendLine(String.Format(strFormat1, "Architecture: ", osInfo.OSArchitecture));
                sb.AppendLine(String.Format(strFormat1, "System Drive: ", osInfo.SystemDrive));
                sb.AppendLine(String.Format(strFormat1, "Windows Directory: ", osInfo.WindowsDirectory)); 

                sb.AppendLine(String.Format(strFormat1, "Install Date: ", osInfo.InstallDate));
                sb.AppendLine(String.Format(strFormat1, "Last Boot Time: ", osInfo.LastBootUpTime));
                sb.AppendLine(String.Format(strFormat1, "Local Time: ", osInfo.LocalDateTime));

                sb.AppendLine(String.Format(strFormat1, "Country: ", osInfo.CountryCode));
                sb.AppendLine(String.Format(strFormat1, "Time Zone: ", osInfo.CurrentTimeZone));
                //sb.AppendLine(String.Format(strFormat1, "Users: ", osInfo.NumberOfUsers));
                sb.AppendLine(String.Format(strFormat1, "Registered User: ", osInfo.RegisteredUser));
                sb.AppendLine(String.Format(strFormat1, "Organization: ", osInfo.Organization));
                sb.AppendLine(String.Format(strFormat1, "Free Physical Memory: ", osInfo.FreePhysicalMemory));
                sb.AppendLine(String.Format(strFormat1, "Free Space In Paging Files: ", osInfo.FreeSpaceInPagingFiles));
                sb.AppendLine(String.Format(strFormat1, "Free Virtual Memory: ", osInfo.FreeVirtualMemory));
                sb.AppendLine(String.Format(strFormat1, "Max Process Memory: ", osInfo.MaxProcessMemorySize));
                sb.AppendLine(String.Format(strFormat1, "Total Virtual Memory: ", osInfo.TotalVirtualMemorySize));
                sb.AppendLine(String.Format(strFormat1, "Total Visible Memory: ", osInfo.TotalVisibleMemorySize));   

                sb.AppendLine(" ");

                #endregion

                #region Antivirus

                // AV
                sb.AppendLine("Antivirus Software");
                sb.AppendLine(strLine);

                if (avInfo != null)
                {
                    sb.AppendLine(String.Format(strFormat, "Name:", avInfo.DisplayName));
                    sb.AppendLine(String.Format(strFormat, "Company: ", avInfo.CompanyName));
                    sb.AppendLine(String.Format(strFormat, "On Access Scanning: ", avInfo.OnAccessScanningEnabled));
                    sb.AppendLine(String.Format(strFormat, "Path: ", avInfo.PathToSignedProductExe));
                    sb.AppendLine(String.Format(strFormat, "State: ", avInfo.ProductState));
                    sb.AppendLine(String.Format(strFormat, "Up to Date: ", avInfo.ProductUptoDate));

                    sb.AppendLine(" ");
                }

                #endregion

                #region Firewall

                sb.AppendLine("Firewall Status");
                sb.AppendLine(strLine);
                sb.AppendLine(FirewallInfo.GetFilrewallInfo());
                //sb.AppendLine(" ");

                #endregion

                #region Windows Update

                sb.AppendLine("Windows Update Status");
                sb.AppendLine(strLine);
                WUApiLib.AutomaticUpdatesClass auc = new WUApiLib.AutomaticUpdatesClass();

                if (auc != null)
                {
                    if (auc.ServiceEnabled == true)
                        sb.AppendLine("Enabled: Yes");
                    else
                        sb.AppendLine("Enabled: No");

                    sb.AppendLine(String.Format(strFormat, "Last Successful Installation:", auc.Results.LastInstallationSuccessDate.ToString()));
                }
                else
                    sb.AppendLine("** Unknown **");

                sb.AppendLine(" ");

                #endregion

                #region CPU

                // CPU                
                sb.AppendLine("CPU");
                sb.AppendLine(strLine);

                if (cpu != null)
                {
                    sb.AppendLine(String.Format(strFormat, "Name:", cpu.Name));
                    sb.AppendLine(String.Format(strFormat, "Description:", cpu.Description));
                    sb.AppendLine(String.Format(strFormat, "Max Clock Speed:", cpu.MaxClockSpeed));
                    sb.AppendLine(String.Format(strFormat, "Manufacturer:", cpu.Manufacturer));
                    sb.AppendLine(String.Format(strFormat, "Cores:", cpu.NumberOfCores));
                    sb.AppendLine(String.Format(strFormat, "Logical Processors:", cpu.NumberOfLogicalProcessors));
                    sb.AppendLine(String.Format(strFormat, "Address Width:", cpu.AddressWidth));
                    sb.AppendLine(String.Format(strFormat, "Data Width:", cpu.DataWidth));
                    sb.AppendLine(String.Format(strFormat, "Architecture:", cpu.Architecture));
                    sb.AppendLine(String.Format(strFormat, "Availability:", cpu.Availability));
                    sb.AppendLine(String.Format(strFormat, "Cpu Status:", cpu.CpuStatus));
                    sb.AppendLine(String.Format(strFormat, "Current Clock Speed:", cpu.CurrentClockSpeed));
                    sb.AppendLine(String.Format(strFormat, "Current Voltage:", cpu.CurrentVoltage));
                    sb.AppendLine(" ");
                }

                #endregion

                #region Memory

                // Memory
                SetStatus("Collecting Memory information...");
                sb.AppendLine("Memory");
                sb.AppendLine(strLine);

                sb.AppendLine(String.Format(strFormat, "Total Memory: ", Memory.GetTotalMemory()));
                sb.AppendLine(String.Format(strFormat, "Available Memory: ", Memory.GetAvailableMemory()));
                sb.AppendLine(" ");
                
                #endregion

                txtSystem.Text = sb.ToString();
            });

            osInfo = null;
            avInfo = null;
            cpu = null;
            
            SetStatus(" ");
            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("System information consumed : {0}", strTime));
        }

        private void bgwHDD_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting HDD information...");
            List<HardDiskInfo> hddInfo = HardDisk.GetHDDInfo();

            this.txtHDD.Invoke((Action)delegate
            {
                #region HDD

                // HDD
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Hard Disk Drive Information");                
                sb.AppendLine("----------------------------------------------------------------------------------------");
                
                string strFormat = "{0, -30} {1, -30}";

                if (hddInfo != null)
                {
                    foreach (var hdd in hddInfo)
                    {
                        sb.AppendLine(String.Format(strFormat, "Name: ", hdd.Name));
                        sb.AppendLine(String.Format(strFormat, "Description: ", hdd.Description));
                        sb.AppendLine(String.Format(strFormat, "Provider Name: ", hdd.ProviderName));
                        sb.AppendLine(String.Format(strFormat, "Drive Type: ", hdd.DriveType));
                        sb.AppendLine(String.Format(strFormat, "File System: ", hdd.FileSystem));
                        sb.AppendLine(String.Format(strFormat, "Free Space: ", hdd.FreeSpace));
                        sb.AppendLine(String.Format(strFormat, "Size: ", hdd.Size));               
                        sb.AppendLine(String.Format(strFormat, "Volume Serial Number: ", hdd.VolumeSerialNumber));
                        sb.AppendLine(" ");                        
                    }
                }

                txtHDD.Text = sb.ToString();
                #endregion 
            });

            hddInfo.Clear();
            SetStatus(" ");
            hddInfo = null;

            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("HDD information consumed : {0}", strTime));
        }

        private void bgwNetwork_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting Network information...");
            List<NetworkInterface> netInterfaces = Network.GetNetInterfaceList();

            this.txtNetInterfaces.Invoke((Action)delegate
            {
                #region Network

                // System
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Network Information");
                sb.AppendLine("----------------------------------------------------------------------------------------");
                
                string strFormat = "{0, -20} {1, -30}";

                if (netInterfaces != null)
                {
                    foreach (var netInterface in netInterfaces)
                    {
                        sb.AppendLine(String.Format(strFormat, "Description: ", netInterface.Description));
                        sb.AppendLine(String.Format(strFormat, "DHCP Enabled: ", netInterface.DHCPEnabled));
                        sb.AppendLine(String.Format(strFormat, "DNS Host Name: ", netInterface.DNSHostName));
                        sb.AppendLine(String.Format(strFormat, "IP Address: ", netInterface.IPAddress));

                        if (string.IsNullOrEmpty(_ip) == true)
                        {
                            _ip = netInterface.IPAddress;
                            PreselectScanIPRange();
                        }

                        sb.AppendLine(String.Format(strFormat, "Subnet: ", netInterface.IPSubnet));
                        sb.AppendLine(String.Format(strFormat, "MAC Address: ", netInterface.MACAddress));
                        sb.AppendLine(" ");
                    }
                }

                txtNetInterfaces.Text = sb.ToString();

                #endregion
            });

            netInterfaces.Clear();
            SetStatus(" ");
            netInterfaces = null;

            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Network information consumed : {0}", strTime));
        }

        private void bgwNetworkConnections_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //EnableControl(btnRefreshNwOpenPorts, false);

            SetStatus("Collecting network connections information...");
            List<RemoteConnection> connections = Network.GetNetStatConnections();

            this.txtNetConnections.Invoke((Action)delegate
            {
                StringBuilder sb = new StringBuilder();

                #region Netstat

                if (connections.Count == 0)
                {
                    txtNetConnections.Text = "The requested operation requires elevation.";
                }
                else
                {
                    sb.AppendLine(String.Format("{0,-10 } {1, -50} {2, -20} {3, -20} {4, -20}", "PID", "Process Name", "Local Address", "Remote Address", "Protocol"));
                    sb.AppendLine("------------------------------------------------------------------------------------------------------------------");

                    foreach (var connection in connections)
                    {
                        sb.AppendLine(String.Format("{0,-10 } {1, -50} {2, -20} {3, -20} {4, -20}", connection.Pid, connection.ProcessName, connection.LocalAddress, connection.RemoteAddress, connection.Protocol));
                    }

                    sb.AppendLine("------------------------------------------------------------------------------------------------------------------");
                    sb.AppendLine("Total " + connections.Count.ToString() + " connections found.");

                    txtNetConnections.Text = sb.ToString();
                }

                #endregion
            });

            connections.Clear();
           // EnableControl(btnRefreshNwOpenPorts);
            SetStatus(" ");
            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Netstat information consumed : {0}", strTime));
        }

        private void bgwInstalledPrograms_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting Installed Programs information...");
            List<InstalledProgramsInfo> ipInfo = InstalledPrograms.GetInstalledProgramsInfo();

            this.txtPrograms.Invoke((Action)delegate
            {
                #region Installed Programs

                string strFormat = "{0, -20} {1, -30}";
                string strLine = "----------------------------------------------------------------------------------------";
                string strLine1 = "-----------------------------------------------------------";
                StringBuilder sb = new StringBuilder();
                
                sb.AppendLine("Installed Programs Information");
                sb.AppendLine(strLine);
                sb.AppendLine(" ");

                if (ipInfo != null)
                {
                    foreach (var info in ipInfo)
                    {
                        sb.AppendLine(info.Name);                        
                        sb.AppendLine(strLine1);

                        sb.AppendLine(String.Format(strFormat, "Description: ", info.Description));
                        sb.AppendLine(String.Format(strFormat, "Version: ", info.Version));                        
                        sb.AppendLine(String.Format(strFormat, "IdentifyingNumber: ", info.IdentifyingNumber));
                        sb.AppendLine(String.Format(strFormat, "InstallDate: ", info.InstallDate));
                        sb.AppendLine(String.Format(strFormat, "InstallLocation: ", info.InstallLocation));
                        sb.AppendLine(String.Format(strFormat, "LocalPackage: ", info.LocalPackage));

                        sb.AppendLine(String.Format(strFormat, "PackageCode: ", info.PackageCode));                        
                        sb.AppendLine(String.Format(strFormat, "PackageName: ", info.PackageName));
                        sb.AppendLine(String.Format(strFormat, "Vendor: ", info.Vendor));
                        sb.AppendLine(" " );
                    }
                }


                txtPrograms.Text = sb.ToString();
                #endregion
            });

            ipInfo.Clear();
            SetStatus(" ");

            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Installed Programs consumed : {0}", strTime));
        }

        private void bgwProcessInfo_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting Process information...");
            List<SystemProcessInfo> procInfo = SystemProcesses.GetProcessInfo();

            this.txtProcesses.Invoke((Action)delegate
            {
                #region Processes
                StringBuilder sb = new StringBuilder();
                string strFormat = "{0, -40} {1, 10} {2, 15} {3, 15} {4, 15}";                
                string strLine = "----------------------------------------------------------------------------------------------------------------";                               
                
                sb.AppendLine(String.Format(strFormat, "Name", "PID", "PPID", "Virtual Size", "Working Set"));
                sb.AppendLine(strLine);

                if (procInfo != null)
                {
                    foreach (var info in procInfo)
                    {
                        sb.AppendLine(String.Format(strFormat, info.Name, info.ProcessId, info.ParentProcessId, info.VirtualSize, info.WorkingSetSize));
                    }

                    sb.AppendLine(strLine);                    
                    sb.AppendLine(String.Format("Total processes: {0}", procInfo.Count));
                }

                txtProcesses.Text = sb.ToString();
                #endregion
            });

            procInfo.Clear();
            SetStatus(" ");

            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Process Information consumed : {0}", strTime));
        }

        private void bgwRuntime_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting Runtime information...");
            List<string> dotNetRT = Runtimes.GetDotNetVersions();

            this.txtRuntime.Invoke((Action)delegate
            {
                #region Runtime

                StringBuilder sb = new StringBuilder();

                string strLine = "----------------------------------------------------------------------------------------";
                sb.AppendLine(".NET Runtime");
                sb.AppendLine(strLine);

                if (dotNetRT != null)
                {
                    foreach (var info in dotNetRT)
                    {
                        sb.AppendLine(info);
                    }
                }

                sb.AppendLine(" ");
                txtRuntime.Text = sb.ToString();

                #endregion
            });

            dotNetRT.Clear();
            SetStatus(" ");

            sw.Stop();
            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Runtime consumed : {0}", strTime));
        }

        private void bgwEnvironmentVars_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            
            SetStatus("Collecting Environment Variable information...");
            string vars = EnvironmentVariables.GetEnvironmentVariables();

            this.txtEnvVariable.Invoke((Action)delegate
            {
                #region Environment Variavles

                txtEnvVariable.Text = vars;

                #endregion
            });

            vars = "";
            SetStatus(" ");
            sw.Stop();

            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Environment Variable information consumed : {0}", strTime));
        }

        private void bgwHotFixes_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting hotfix information...");
            List<string> hotFixes = WindowsUpdates.GetWindowsUpdates();

            this.txtHotFixes.Invoke((Action)delegate
            {
                #region Hot Fixes

                StringBuilder sb = new StringBuilder();
                string strLine = "----------------------------------------------------------------------------------------";
                sb.AppendLine("Hotfixes");
                sb.AppendLine(strLine);
                sb.AppendLine(" ");

                foreach (string hotfix in hotFixes)
                {
                    sb.AppendLine(hotfix);
                }

                sb.AppendLine(strLine);
                sb.Append("Total Hotfixes: ");
                sb.Append(hotFixes.Count.ToString());

                txtHotFixes.Text = sb.ToString();
                #endregion
            });

            hotFixes.Clear();
            SetStatus(" ");
            sw.Stop();

            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Hotfix information consumed : {0}", strTime));
        }

        private void bgwServices_DoWork(object sender, DoWorkEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            SetStatus("Collecting services information...");
            List<SystemServiceInfo> serviceInfo = Services.GetServicesInfo();

            this.txtServices.Invoke((Action)delegate
            {
                #region Services

                StringBuilder sb = new StringBuilder();
                string strFormat = "{0, -10} {1, -40} {2, -5} {3, -30} {4, -40}";
                string strLine = "----------------------------------------------------------------------------------------------------------------";
                sb.AppendLine(String.Format(strFormat, "Status", "Name", "PID", "Log On As", "Command Line"));
                sb.AppendLine(strLine);

                if (serviceInfo != null)
                {
                    foreach (var info in serviceInfo.Where(s => s.State == "Running"))
                    {
                        sb.AppendLine(String.Format(strFormat, info.State, info.Name.Truncate(37), info.ProcId.ToString(), info.User, info.CommandLine));
                    }

                    foreach (var info in serviceInfo.Where(s => s.State == "Stopped"))
                    {
                        sb.AppendLine(String.Format(strFormat, info.State, info.Name.Truncate(37), info.ProcId.ToString(), info.User, info.CommandLine));
                    }

                    sb.AppendLine(strLine);
                    sb.AppendLine(String.Format("Total services: {0}", serviceInfo.Count));
                }

                txtServices.Text = sb.ToString();

                #endregion
            });

            serviceInfo.Clear();
            SetStatus(" ");
            sw.Stop();

            string strTime = sw.Elapsed.ToString("mm\\:ss\\.fff");
            AddDebug(String.Format("Services information consumed : {0}", strTime));
        } 

        #region Button Handling

        private void btnOpenSystem_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\System32\control.exe", "system");
            }
            catch (Exception exp)
            {
                ShowError("OpenSystem: " + exp.Message);
            }
        }

        private void btnOpenSysInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\System32\msinfo32.exe");
            }
            catch (Exception exp)
            {
                ShowError("OpenSysInfo: " + exp.Message);
            }
        }

        private void btnOpenFirewall_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.WindowsFirewall");
            }
            catch (Exception exp)
            {
                ShowError("OpenFirewall: " + exp.Message);
            }
        }

        private void btnWinUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.WindowsUpdate");
            }
            catch (Exception exp)
            {
                ShowError("WinUpdate: " + exp.Message);
            }
        }

        private void btnDiskCleanup_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\System32\cleanmgr.exe");
            }
            catch (Exception exp)
            {
                ShowError("DiskCleanup: " + exp.Message);
            }
        }

        private void btnDiskDefrag_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\System32\dfrgui.exe");
            }
            catch (Exception exp)
            {
                ShowError("DiskDefrag: " + exp.Message);
            }
        }

        private void btnNWCEnter_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.NetworkAndSharingCenter");
            }
            catch (Exception exp)
            {
                ShowError("NWCEnter: " + exp.Message);
            }
        }

        private void btnTaskManager_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"taskmgr");
            }
            catch (Exception exp)
            {
                ShowError("TaskManager: " + exp.Message);
            }
        }

        private void btnResourceMonitor_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"perfmon", "/res");
            }
            catch (Exception exp)
            {
                ShowError("ResourceMonitor: " + exp.Message);
            }
        }

        private void btnRelbMonitor_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"perfmon", "/rel");
            }
            catch (Exception exp)
            {
                ShowError("RelbMonitor: " + exp.Message);
            }
        }

        private void btnSystemReport_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"perfmon", "/report");
            }
            catch (Exception exp)
            {
                ShowError("SystemReport: " + exp.Message);
            }
        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo("ping", txtPingHost.Text);

                info.UseShellExecute = true;
                info.CreateNoWindow = false;
                Process p = new Process();

                p.StartInfo = info;
                p.Start();
            }
            catch (Exception exp)
            {
                ShowError("Ping: " + exp.Message);
            }
        }

        private void btnPathPing_Click(object sender, EventArgs e)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo("cmd.exe", "/K pathping " + txtPingHost.Text);

                info.UseShellExecute = true;
                info.CreateNoWindow = false;
                Process p = new Process();
                
                p.StartInfo = info;
                p.Start();
            }
            catch (Exception exp)
            {
                ShowError("PathPing: " + exp.Message);
            }
        }  

        private void btnIpConfig_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand("cmd.exe", "/K ipconfig");
            }
            catch (Exception exp)
            {
                ShowError("IpConfig: " + exp.Message);
            }
        }  

        private void btnOpenHostsFile_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand("notepad.exe", @"C:\Windows\System32\drivers\etc\hosts");
            }
            catch (Exception exp)
            {
                ShowError("OpenHostsFile: " + exp.Message);
            }            
        }

        private void btnSystemMaintenance1_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\System32\msdt.exe", "-ep TSControlPanel -id MaintenanceDiagnostic -context {30E37447-FC7E-4E17-A375-4C8401139DCC}");
            }
            catch (Exception exp)
            {
                ShowError("SystemMaintenance: " + exp.Message);
            }
        }

        private void btnPerformDiag_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\System32\msdt.exe", @"-ep TSControlPanel -path C:\Windows\Diagnostics\Index\PerformanceDiagnostic.xml -context {804325B7-DB5C-42FE-98F4-C21732ECCBA4}");
            }
            catch (Exception exp)
            {
                ShowError("PerformDiag: " + exp.Message);
            }
        }

        private void btnPSR_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"psr");
            }
            catch (Exception exp)
            {
                ShowError("PSR: " + exp.Message);
            }
        }

        private void btnTroubleshooting_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.Troubleshooting");
            }
            catch (Exception exp)
            {
                ShowError("Troubleshooting: " + exp.Message);
            }
        }

        private void btnAdminTools_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.AdministrativeTools");
            }
            catch (Exception exp)
            {
                ShowError("AdminTools: " + exp.Message);
            }
        }

        private void btnDeviceMgr_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.DeviceManager");
            }
            catch (Exception exp)
            {
                ShowError("DeviceMgr: " + exp.Message);
            }
        }

        private void btnEventViewer_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\system32\eventvwr.msc", "/s");
            }
            catch (Exception exp)
            {
                ShowError("EventViewer: " + exp.Message);
            }
        }

        private void btnPerfMon_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"perfmon", "/sys");
            }
            catch (Exception exp)
            {
                ShowError("PerfMon: " + exp.Message);
            }
        }

        private void btnControlPanel_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control");
            }
            catch (Exception exp)
            {
                ShowError("ControlPanel: " + exp.Message);
            }
        }

        private void btnFolderOptions_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.FolderOptions");
            }
            catch (Exception exp)
            {
                ShowError("FolderOptions: " + exp.Message);
            }
        }

        private void btnRemoteDesktop_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"mstsc");
            }
            catch (Exception exp)
            {
                ShowError("RemoteDesktop: " + exp.Message);
            }
        }

        private void btnAboutWindows_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\system32\winver.exe");
            }
            catch (Exception exp)
            {
                ShowError("AboutWindows: " + exp.Message);
            }
        }

        private void btnSysConfig_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"msconfig", "", @"C:\Windows\System32");
                //Utility.ExecuteCommand(@"C:\Windows\System32\msconfig.exe");
            }
            catch (Exception exp)
            {
                ShowError("SysConfig: " + exp.Message);
            }
        }

        private void btnMasterControlPanel_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"explorer.exe", "shell:::{ED7BA470-8E54-465E-825C-99712043E01C}");
            }
            catch (Exception exp)
            {
                ShowError("MasterControlPanel: " + exp.Message);
            }
        }

        private void btnCmd_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"cmd.exe");
            }
            catch (Exception exp)
            {
                ShowError("cmd: " + exp.Message);
            }
        }

        private void btnUninstall_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "appwiz.cpl");
            }
            catch (Exception exp)
            {
                ShowError("Uninstall: " + exp.Message);
            }
        }

        private void btnBackupAndRestore_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "/name Microsoft.BackupAndRestoreCenter");
            }
            catch (Exception exp)
            {
                ShowError("BackupAndRestore: " + exp.Message);
            }
        }

        private void btnRefreshNwOpenPorts_Click(object sender, EventArgs e)
        {
            bgwNetworkPorts.RunWorkerAsync();
        }

        private void btnCredentialManager_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"control", "keymgr.dll");
            }
            catch (Exception exp)
            {
                ShowError("CredentialManager: " + exp.Message);
            }            
        }

        private void btnMobilityCenter_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"mblctr.exe");
            }
            catch (Exception exp)
            {
                ShowError("MobilityCenter: " + exp.Message);
            }    
        }

        private void btnRegedit_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"regedit.exe");
            }
            catch (Exception exp)
            {
                ShowError("Regedit: " + exp.Message);
            }    
        }

        private void btnServices_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.ExecuteCommand(@"C:\Windows\system32\services.msc");
            }
            catch (Exception exp)
            {
                ShowError("Services: " + exp.Message);
            }    
        }   

        #endregion

        private void EnableControl(Control control, bool bEnable = true)
        {
            control.Invoke((Action)delegate
            {
                control.Enabled = bEnable;
            });
        }

        private void ShowError(string err)
        {
            StringBuilder sb = new StringBuilder();

            this.txtError.Invoke((Action)delegate
            {
                sb.AppendLine(err);
                sb.AppendLine("---------------------------------------------------------------------------");

                txtError.Text += sb.ToString();
            });
        }

        private void timerPing_Tick(object sender, EventArgs e)
        {
            // Check 3 hosts
            int connectivity = Network.PingHost("www.google.com") == true ? 1 : 0;
            connectivity += Network.PingHost("www.microsoft.com") == true ? 1 : 0;
            connectivity += Network.PingHost("www.wikipedia.com") == true ? 1 : 0;

            if (connectivity >= 2)
            {
                lblInternet.Text = "Internet Access";
                lblInternet.BackColor = Color.GreenYellow;
                _bInternetConnected = true;
            }
            else
            {
                lblInternet.Text = "No Internet Access";
                lblInternet.BackColor = Color.Red;
                _bInternetConnected = false;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bgwHDD.IsBusy == true)
                bgwHDD.CancelAsync();
            if (bgwInstalledPrograms .IsBusy == true)
                bgwInstalledPrograms.CancelAsync();
            if (bgwNetwork.IsBusy == true)
                bgwNetwork.CancelAsync();
            if (bgwNetworkPorts.IsBusy == true)
                bgwNetworkPorts.CancelAsync();
            if (bgwOS.IsBusy == true)
                bgwOS.CancelAsync();
            if (bgwProcessInfo.IsBusy == true)
                bgwProcessInfo.CancelAsync();
            if (bgwProcessInfo.IsBusy == true)
                bgwProcessInfo.CancelAsync();
            if (bgwRuntime.IsBusy == true)
                bgwRuntime.CancelAsync();
            if (bgwHotFixes.IsBusy == true)
                bgwHotFixes.CancelAsync();
            
            timerPing.Stop();
        }

        private void SetStatus(string strStatus)
        {
            this.lblStatus.Invoke((Action)delegate
            {
                lblStatus.Text = strStatus;
            });
        }

        private void AddDebug(string strDebug)
        {
            this.txtDebug.Invoke((Action)delegate
            {
                txtDebug.Text += strDebug;
                txtDebug.Text += Environment.NewLine;
            });
        }

        private void txtPingHost_TextChanged(object sender, EventArgs e)
        {
            btnPathPing.Enabled = btnPing.Enabled = txtPingHost.Text.Length > 1;
        }

        private void PreselectScanIPRange()
        {
            if (string.IsNullOrEmpty(_ip) == false)
            {
                string ip = _ip.Remove(_ip.LastIndexOf('.'));
                ipControlFrom.IP = ip;
                ipControlTo.IP = ip + ".255";
            }            
        }

        private void btnCopyToCLipboard_Click(object sender, EventArgs e)
        {
            string text = "";

            switch (tabControl1.SelectedTab.Text)
            {
                case "System":
                    if (string.IsNullOrEmpty(txtSystem.Text) == false)
                    {
                        text = txtSystem.Text;
                    }
                    break;

                case "Startup Items":
                if (string.IsNullOrEmpty(txtSystem.Text) == false)
                {
                    text = txtStartupItems.Text;
                }
                break;

                case "HDD":
                if (string.IsNullOrEmpty(txtHDD.Text) == false)
                {
                    text = txtHDD.Text;
                }
                break;

                case "Networking":

                switch (tabControl2.SelectedTab.Text)
                {
                    case "Interfaces":
                    if (string.IsNullOrEmpty(txtNetInterfaces.Text) == false)
                    {
                        text = txtNetInterfaces.Text;
                    }
                    break;

                    case "Open Ports":
                    if (string.IsNullOrEmpty(txtNetPorts.Text) == false)
                    {
                        text = txtNetPorts.Text;
                    }
                    break;

                    case "Connections":
                    if (string.IsNullOrEmpty(txtNetConnections.Text) == false)
                    {
                        text = txtNetConnections.Text;
                    }
                    break;
                }
                
                break;

                case "Processes":
                if (string.IsNullOrEmpty(txtProcesses.Text) == false)
                {
                    text = txtProcesses.Text;
                }
                break;

                case "Services":
                if (string.IsNullOrEmpty(txtServices.Text) == false)
                {
                    text = txtServices.Text;
                }
                break;

                case "Runtimes":
                if (string.IsNullOrEmpty(txtRuntime.Text) == false)
                {
                    text = txtRuntime.Text;
                }
                break;

                case "Environment Variables":
                if (string.IsNullOrEmpty(txtEnvVariable.Text) == false)
                {
                    text = txtEnvVariable.Text;
                }
                break;

                case "Installed Software":
                if (string.IsNullOrEmpty(txtPrograms.Text) == false)
                {
                    text = txtPrograms.Text;
                }
                break;

                case "Hot Fixes":
                if (string.IsNullOrEmpty(txtHotFixes.Text) == false)
                {
                    text = txtHotFixes.Text;
                }
                break;
            }
            Clipboard.SetText(text, TextDataFormat.Text);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnCopyToCLipboard.Enabled = (tabControl1.SelectedTab != tabAbout && tabControl1.SelectedTab != tabTools);
        }

        #region Save Data

        private void btnSaveAll_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SaveAllToFiles(folderBrowserDialog.SelectedPath);
            }
        }

        private void SaveAllToFiles(string path)
        {
            SaveToFile(_bInternetConnected ? "Internet connection available." : "No internet connection.", path + "\\Internet.txt");
            SaveToFile(txtSystem.Text, path + "\\System.txt");
            SaveToFile(txtStartupItems.Text, path + "\\StartupItems.txt");
            
            SaveToFile(txtHDD.Text, path + "\\HDD.txt");

            SaveToFile(txtNetInterfaces.Text, path + "\\Network.txt");
            SaveToFile(txtNetPorts.Text, path + "\\Network.txt");
            SaveToFile(txtNetConnections.Text, path + "\\Network.txt");

            SaveToFile(txtProcesses.Text, path + "\\Processes.txt");
            SaveToFile(txtServices.Text, path + "\\Services.txt");
            SaveToFile(txtRuntime.Text, path + "\\Runtime.txt");
            SaveToFile(txtEnvVariable.Text, path + "\\EnvVariable.txt");
            SaveToFile(txtPrograms.Text, path + "\\Programs.txt");
            SaveToFile(txtHotFixes.Text, path + "\\HotFixes.txt");

            Utility.ExecuteCommand("explorer.exe", path);
        }

        private void SaveToFile(string text, string file)
        {
            using (StreamWriter writer = new StreamWriter(file))
            {
                writer.Write(text);
                writer.Close();
            }
        }

        #endregion

        private void lnkLblAvira_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Utility.ExecuteCommand("http://www.avira.com/en/download/product/avira-rescue-system");
            }
            catch (Exception exp)
            {
                ShowError("Avira: " + exp.Message);
            }
        }

        private void Kaspersky_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Utility.ExecuteCommand("http://support.kaspersky.com/viruses/rescuedisk");
            }
            catch (Exception exp)
            {
                ShowError("Kaspersky: " + exp.Message);
            }
        }

        private void lnkLblOther_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Utility.ExecuteCommand("https://www.google.com.au/webhp?sourceid=ie-instant&ion=1&espv=2&ie=UTF-8#safe=active&q=antivirus+live+disk");
            }
            catch (Exception exp)
            {
                ShowError("Other: " + exp.Message);
            }
        }
    }
}
