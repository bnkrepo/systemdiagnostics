﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace System_Diagnostics
{
    public class Antivirus
    {
        public static AntivirusInfo GetAVInfo()
        {
            try
            {
                ManagementObjectSearcher mos = null;

                //Windows Vista/7/8
                if (Environment.OSVersion.Version.Major > 5)
                {
                    mos = new ManagementObjectSearcher(@"\\" + Environment.MachineName + @"\root\SecurityCenter2", "SELECT * FROM AntivirusProduct");
                }
                //Windows XP
                else
                {
                    mos = new ManagementObjectSearcher(@"\\" + Environment.MachineName + @"\root\SecurityCenter", "SELECT * FROM AntivirusProduct");
                }

                ManagementObjectCollection moc = mos.Get();

                foreach (ManagementObject mo in moc)
                {
                    AntivirusInfo avInfo = new AntivirusInfo();
                    try { avInfo.DisplayName = mo["displayName"].ToString(); } catch { }
                    try { avInfo.CompanyName = mo["companyName"].ToString(); } catch { }
                    try { avInfo.InstanceGuid = mo["instanceGuid"].ToString(); } catch { }
                    try { avInfo.OnAccessScanningEnabled = mo["onAccessScanningEnabled"].ToString(); } catch { }
                    try { avInfo.PathToSignedProductExe = mo["pathToSignedProductExe"].ToString(); } catch { }
                    try { avInfo.ProductHasNotifiedUser = mo["productHasNotifiedUser"].ToString(); } catch { }
                    try { avInfo.ProductState = mo["productState"].ToString(); } catch { }
                    try { avInfo.ProductUptoDate = mo["productUptoDate"].ToString(); } catch { }
                    try { avInfo.VersionNumber = mo["versionNumber"].ToString(); } catch { }

                    return avInfo;
                }

            }
            catch (ManagementException)
            {
                return null;
            }

            return null;
        }
    }

    public class AntivirusInfo
    {
        public string DisplayName { get; set; }
        public string CompanyName { get; set; }
        public string InstanceGuid { get; set; }
        public string OnAccessScanningEnabled { get; set; }
        public string PathToSignedProductExe { get; set; }
        public string ProductHasNotifiedUser { get; set; }
        public string ProductState { get; set; }
        public string ProductUptoDate { get; set; }
        public string VersionNumber { get; set; }

        public AntivirusInfo()
        {
            DisplayName = "-";
            CompanyName = "-";
            InstanceGuid = "-";
            OnAccessScanningEnabled = "-";
            PathToSignedProductExe = "-";
            ProductHasNotifiedUser = "-";
            ProductState = "-";
            ProductUptoDate = "-";
            VersionNumber = "-";            
        }
    }
}
