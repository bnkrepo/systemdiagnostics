﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace System_Diagnostics
{
    public class SystemProcesses
    {
        public static List<SystemProcessInfo> GetProcessInfo()
        {
            List<SystemProcessInfo> processInfo = new List<SystemProcessInfo>();

            try
            {                
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Process");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemProcessInfo info = new SystemProcessInfo();

                    info.Name               = queryObj["Name"].ToString();
                    try { info.CommandLine = queryObj["CommandLine"].ToString(); } catch (Exception) { info.CommandLine = ""; }
                    info.ParentProcessId    = Int32.Parse(queryObj["ParentProcessId"].ToString());
                    info.ProcessId          = Int32.Parse(queryObj["ProcessId"].ToString());
                    info.VirtualSize        = Utility.FormatBytes((ulong)Int64.Parse(queryObj["VirtualSize"].ToString()));
                    info.WorkingSetSize     = Utility.FormatBytes((ulong)Int64.Parse(queryObj["WorkingSetSize"].ToString()));

                    processInfo.Add(info);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return processInfo;
        }
    }

    public class SystemProcessInfo
    {
        public string Name { get; set; }
        public string CommandLine { get; set; }
        public int ParentProcessId { get; set; }
        public int ProcessId { get; set; }
        public string VirtualSize { get; set; }
        public string WorkingSetSize { get; set; }

        public SystemProcessInfo()
        {

        }
    }
}
