﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace System_Diagnostics
{
    public class Services
    {
        public static List<SystemServiceInfo> GetServicesInfo()
        {
            List<SystemServiceInfo> servicesInfo = new List<SystemServiceInfo>();

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Service");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemServiceInfo info = new SystemServiceInfo();

                    info.Name = queryObj["Caption"].ToString().Trim();
                    try { info.CommandLine = queryObj["PathName"].ToString().Trim(); }
                    catch (Exception) { info.CommandLine = ""; }
                    info.ProcId = Int32.Parse(queryObj["ProcessId"].ToString());
                    info.User = queryObj["StartName"].ToString().Trim();
                    info.State = queryObj["State"].ToString().Trim();

                    servicesInfo.Add(info);
                }
            }
            catch (ManagementException)
            {
                return null;
            }

            return servicesInfo;
        }
    }

    public class SystemServiceInfo
    {
        public string Name { get; set; }
        public string CommandLine { get; set; }
        public int ProcId { get; set; }
        public string State { get; set; }
        public string User { get; set; }

        public SystemServiceInfo()
        {

        }
    }
}
